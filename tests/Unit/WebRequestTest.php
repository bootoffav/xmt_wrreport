<?php

namespace Tests\Unit;

use App\WebRequest;
use PHPUnit\Framework\TestCase;

class WebRequestTest extends TestCase
{
    /**
     * Test sort function of WebRequest
     */
    public function test_sorting_web_requests(): void
    {
        $new = (object) [
            'TITLE' => 'Web_request #4523 from ТОО «Самрук-Казына Контракт» (Kazakhstan) g.rakhimova@skc.kz (22 January 2024)',
            'RESPONSIBLE_ID' => '9',
            'ID' => '128725',
            'CREATED_BY' => '189',
            'CREATED_DATE' => '2024-01-22T14:47:04+03:00',
            'country' => 'Kazakhstan',
            'revenue' => null,
            'brands' => [],
            'products' => [],
            'companies' => null,
        ];
        $old = (object) [
            'TITLE' => 'Web_request #4512 from Annas Uniformes Sanitarios (Spain) annas@annas.com.ar (18 January 2024)',
            'RESPONSIBLE_ID' => '5306',
            'ID' => '128471',
            'CREATED_DATE' => '2024-01-18T08:53:04+03:00',
            'country' => 'Spain',
            'revenue' => null,
            'brands' => [],
            'products' => [],
            'companies' => [
                'Annas Uniformes Sanitarios',
            ],

        ];

        [$newer, $older] = WebRequest::sort([$old, $new], 'date', 'desc');
        $this->assertEquals('128725', $newer->ID);
        $this->assertEquals('128471', $older->ID);
    }
}
