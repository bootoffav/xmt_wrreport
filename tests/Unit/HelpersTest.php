<?php

namespace Tests\Unit;

use App\Helpers\Formatters;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class HelpersTest extends TestCase
{
    /**
     * @test
     */
    public function formatDate(): void
    {
        // happy paths
        $november2023 = Formatters::formatDate('2023-11-01:2023-11-30');
        $this->assertEquals('November 2023', $november2023);

        $Sep23Nov23 = Formatters::formatDate('2023-09-01:2023-11-30');
        $this->assertEquals('September 2023 - November 2023', $Sep23Nov23);

        $randomDates = Formatters::formatDate('2022-06-13:2023-10-17');
        $this->assertEquals('13 Jun 2022 - 17 Oct 2023', $randomDates);

        $cd = Carbon::createFromFormat('Y-m-d', '2023-12-26');
        $this->assertEquals('26 Dec 2023',
            Formatters::formatDate($cd)
        );

        // failures
        $this->assertNull(
            Formatters::formatDate()
        );
    }
}
