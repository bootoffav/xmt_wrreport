<?php

namespace Tests\Unit;

use App\Caching;
use PHPUnit\Framework\TestCase;

class CachingTest extends TestCase
{
    public function test_formsCorrectProductFromDescription(): void
    {
        $descNoEnglish = 'Request: sarebbe possibile avere 1 metro di questo tessuto? quanto costa?
****************************************************
BUILDER-240  - Scegliete i colori per richiedere un preventivo: Black #1  | 1
****************************************************
';

        $descEnglish = 'Request from XMT-XMF-XMS - Rebirth of Fashion (Romania) gigi.anghel@tbw.ro #253
****************************************************
BUILDER-240  - Choose Colours to Request a Quote: Black #1  | 1
****************************************************
Gigi Anghel';

        $descRus = 'Request from XMT-XMF-XMS - Rebirth of Fashion (Romania) gigi.anghel@tbw.ro #253
****************************************************
BUILDER-240  - Выберите Colours to Request a Quote: Black #1  | 1
****************************************************
Gigi Anghel';

        $descEnglish2 = 'Request from XMT-XMF-XMS - JetMS Completions (United Kingdom) takudzwa.mubaira@jetms.aero #250
****************************************************
FR-OXFORD  - Choose Colours to Request a Quote: Black #128-01  | 1
****************************************************

https://www.xmtextiles.com/request-quote/
IP: 172.71.242.19';

        $descEnglish3 = 'Request from XMT-XMF-XMS - Olteks LLC (Ukraine) i.demchenko326@gmail.com #565
****************************************************
FR-Pique-230  - Choose Colours to Request a Quote: Dark Navy #124-01  | 1
****************************************************';

        $descEnglish4 = 'Fwd: Quote request
        From: Fabrics for Workwear and Uniforms | XM Textiles <mail@xmtextiles.com>
        > Subject: Quote request
        > Date: 1 July 2022, 17:09:29 GMT+3
        > To: web_request@xmtextiles.com
        > Reply-To: info@guineapet.com
        >
        ****************************************************
        RAINBLOCK-140 | 1
        GABRIELA | 1
        ****************************************************
        >
        > Name: Nicolas Pierre Cagica Rapaz
        > Email: info@guineapet.com
        > Сompany: Guibeapet®
        > Country: Portugal
        > Phone: 966229297
        > Request: Hi there, we are looking for softshell fabric or a similar
        > waterproof fabric. By the roll with a width of 150/160cm, from 100 to
        > 350 gsm, machine washable at 30º. We appreciate any suggestions you
        > may have. Please send us the MOQ and prices for the products selected.
        > Thank you.
        >';

        $this->assertEquals(Caching::getPRoductFromDescription($descEnglish4), ['RAINBLOCK-140']);
        $this->assertEquals(Caching::getPRoductFromDescription($descEnglish3), ['FR-Pique-230']);
        $this->assertEquals(Caching::getPRoductFromDescription($descEnglish2), ['FR-OXFORD']);
        $this->assertEquals(Caching::getProductFromDescription($descNoEnglish), ['BUILDER-240']);
        $this->assertEquals(Caching::getProductFromDescription($descEnglish), ['BUILDER-240']);
        $this->assertEquals(Caching::getProductFromDescription($descRus), ['BUILDER-240']);

    }
}
