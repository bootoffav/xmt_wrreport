<?php

use App\Mail\DepartmentReport;
use App\Mail\RequestsForPeriod;
use App\Mail\RequestsForPeriodToUser;
use App\WebRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

Artisan::command('cache {item}', function ($item) {
    call_user_func('App\Caching::cache'.$item);
})->purpose('Cache application specific data');

Artisan::command('updateDepartments', function () {
    $wr = new WebRequest;
    $wr->inDep();
})->purpose('Update users in departments');

Artisan::command('sendRequests {--period=} {--rcpt=*}', function () {
    $recipients = $this->option('rcpt') ?: config('app.requests_recipients');
    $period = $this->option('period');

    $requestsForPeriod = new RequestsForPeriod(
        $period,
        new Request
    );

    Mail::to($recipients)->send($requestsForPeriod);

    // for every user send their personal requests
    foreach ($requestsForPeriod->getUniqueResponsibles() as $respId) {
        if ($respId === '189') {
            continue;
        } // exclude web_request@xmtextiles.com
        $user = cache('realName_id')[$respId];
        $reqsOfUser = array_filter(
            $requestsForPeriod->getRequests(),
            fn ($r) => $r->RESPONSIBLE_ID === $respId
        );
        $prevReqsOfUser = array_filter(
            $requestsForPeriod->rpc->prevRequests,
            fn ($r) => $r->RESPONSIBLE_ID === $respId
        );
        $rfptu = new RequestsForPeriodToUser(
            $user['NAME'].': '.$requestsForPeriod->getSubject(), // subject,
            $respId,
            $reqsOfUser, // requests,
            $prevReqsOfUser, // prevRequests
            $period
        );

        try {
            Mail::to($user['EMAIL'])
            ->cc('vit@xmtextiles.com')
            ->bcc('admin@xmtextiles.com')
            ->send($rfptu);
        } catch (\Exception $th) {
            var_dump($respId, $user['EMAIL']);
        }
    }

})->purpose('sends Requests for period (default=week)');

Artisan::command('sendDepartmentReport {--period=}', function () {
    $period = $this->option('period')
        ?: Carbon::now()->startOfMonth()->toDateString().':'.Carbon::now()->endOfMonth()->toDateString();

    session(['datefilter' => $period]);
    Mail::to(config('app.requests_recipients'))->send(new DepartmentReport(strstr($period, ':', true)));
});
