<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DatesController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OptionsController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\RequestsToEmailController;
use App\Http\Middleware\CheckApproved;

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::middleware([CheckApproved::class])->group(function () {
        Route::controller(HomeController::class)->group(function () {
            Route::get('/approval', 'approval')
                ->name('approval')
                ->withoutMiddleware([CheckApproved::class]);
            Route::get('/', 'summary');
            Route::get('requestedProducts', 'requestedProducts');
            Route::get('/update', 'update')->name('update');
            Route::get('/updateAll', 'updateAll')->name('updateAll');
            Route::get('user/{id}', 'requestsOfUser')->name('requestsOfUser');
            Route::get('year/all', 'requestsOfAllYears');
            Route::get('year/{year}', 'requestsOfYear');
            Route::get('month/{year}-{month}', 'requestsOfMonth');
            Route::get('month/all', 'requestsOfAllMonths');
            Route::get('sale/all', 'sale');
            Route::get('sale/{year}-{month}', 'salesOfMonth');
            Route::get('sale/month/all', 'salesOfAllMonths');
            Route::get('sale/year/all', 'salesOfAllYears');
            Route::get('sale/year/{year}', 'salesOfYear');
            Route::get('source/{site}', 'fromSites');
        });

        Route::controller(DepartmentController::class)->group(function () {
            Route::get('department/all', 'all');
            Route::get('department/export', 'export')->name('department_report');
            Route::get('department/{dep}', 'fromDep');
        });

        Route::get('requests', [RequestController::class, 'all'])->name('requests');

        // Requests to Emails
        Route::controller(RequestsToEmailController::class)->group(function () {
            Route::get('requestsToEmail', 'requestsToEmail');
            Route::post('requestsOfUserToEmail', 'requestsOfUserToEmail');
            Route::post('requestsOfDepartmentToEmail', 'requestsOfDepartmentToEmail');
            Route::post('requestsOfPeriodToEmail', 'requestsOfPeriodToEmail');
        });

        Route::post('options', OptionsController::class)->name('options');
        Route::match(['get', 'post'], 'dates', DatesController::class)->name('dates');
        Route::get('product/{product}', ProductController::class)->where('product', '.*')->name('product');
        Route::get('country/{country}', CountryController::class)->name('country');

        Route::get('companies', CompanyController::class)->name('companies');
    });
});
