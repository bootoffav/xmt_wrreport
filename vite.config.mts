import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import react from "@vitejs/plugin-react";

export default defineConfig({
  plugins: [
    laravel({
      input: [
        "resources/assets/sass/app.scss",
        "resources/sass/app.scss",
        "resources/assets/js/app.tsx",
      ],
      refresh: true,
    }),
    react(),
  ],
  resolve: {
    alias: [
      {
        find: /^~(.*)$/,
        replacement: "$1",
      },
    ],
  },
  build: {
    target: "esnext",
  },
});
