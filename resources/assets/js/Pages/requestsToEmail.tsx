import Tabs from "./RequestsToEmail/Tabs";
import RequestsOfManager from "./RequestsToEmail/RequestsOfManager";
import RequestsForPeriod from "./RequestsToEmail/RequestsForPeriod";
import RequestsOfDepartment from "./RequestsToEmail/RequestsOfDepartment";
import { createContext, useState } from "react";
import { types } from "./RequestsToEmail/TypeSelector";

export const TypeSelectorContext = createContext(null);

type Props = {
  managers: {
    [key: number]: string;
  };
  recipients: string[];
  departments: {
    [key: number]: string;
  };
};

export default function RequestsToEmail({
  managers,
  recipients,
  departments,
}: Props) {
  const [TSState, setTSState] = useState({
    manager: Object.keys(types),
    department: Object.keys(types),
    period: Object.keys(types),
  });

  const updateTSState = ({ currentTarget: { id, checked } }) => {
    const [tab, checkbox] = id.split("-");
    setTSState((s) => ({
      ...s,
      [tab]: checked
        ? [...s[tab], checkbox]
        : s[tab].filter((curType: keyof typeof types) => curType !== checkbox),
    }));
  };

  return (
    <div className="container">
      <TypeSelectorContext.Provider
        value={{
          TSState,
          updateTSState,
        }}
      >
        <Tabs>
          <RequestsOfManager
            managers={Object.entries(managers)}
            remoteURL={globalThis.location.origin + "/requestsOfUserToEmail"}
          />
          <RequestsOfDepartment
            departments={departments}
            remoteURL={
              globalThis.location.origin + "/requestsOfDepartmentToEmail"
            }
          />
          <RequestsForPeriod recipients={recipients} />
        </Tabs>
      </TypeSelectorContext.Provider>
    </div>
  );
}
