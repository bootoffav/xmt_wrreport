globalThis.websiteRequestType = "website";
globalThis.enabledPeriod = "years";

export const showRequestSources = ({ target }) => {
  const { area } = target.dataset;
  target.classList.add("active-a");
  document.getElementById(area).style.display = "initial";

  // change color by toggling class on a proper tag
  if (area === "website-chart-area") {
    globalThis.websiteRequestType = "website";
    target.nextElementSibling.classList.remove("active-a");
  } else {
    globalThis.websiteRequestType = "email";
    target.previousElementSibling.classList.remove("active-a");
  }

  document.getElementById(
    area === "website-chart-area" ? "email-chart-area" : "website-chart-area"
  ).style.display = "none";
};

export const showPeriod = (event, enabledPeriod, disabledPeriod) => {
  globalThis.enabledPeriod = enabledPeriod;
  if (!Array.from(event.currentTarget.classList).includes("active-a")) {
    event.currentTarget.classList.add("active-a");
  }
  document
    .getElementById(`${disabledPeriod}-period-link`)
    .classList.remove("active-a");

  document.getElementById(
    `${globalThis.websiteRequestType}-${enabledPeriod}-period`
  ).style.display = "initial";
  document.getElementById(
    `${globalThis.websiteRequestType}-${disabledPeriod}-period`
  ).style.display = "none";

  document.getElementById(
    `${globalThis.websiteRequestType}${enabledPeriod}-chart`
  ).style.display = "initial"; // websiteyears-chart
  document.getElementById(
    `${globalThis.websiteRequestType}${disabledPeriod}-chart`
  ).style.display = "none";
};

export const show = function ({ target }) {
  const { area } = target.dataset;

  switch (area) {
    case "years-chart-area":
      document.getElementById("months-chart-area").style.display = "none";
      document.getElementById("quarters-chart-area").style.display = "none";
      break;
    case "months-chart-area":
      document.getElementById("years-chart-area").style.display = "none";
      document.getElementById("quarters-chart-area").style.display = "none";
      break;
    case "quarters-chart-area":
      document.getElementById("years-chart-area").style.display = "none";
      document.getElementById("months-chart-area").style.display = "none";
    case "yearsSale-chart-area":
      document.getElementById("monthsSale-chart-area").style.display = "none";
      document.getElementById("quartersSale-chart-area").style.display = "none";
      break;
    case "monthsSale-chart-area":
      document.getElementById("yearsSale-chart-area").style.display = "none";
      document.getElementById("quartersSale-chart-area").style.display = "none";
      break;
    case "quartersSale-chart-area":
      document.getElementById("yearsSale-chart-area").style.display = "none";
      document.getElementById("monthsSale-chart-area").style.display = "none";
  }

  document.getElementById(area).style.display = "initial";
};
