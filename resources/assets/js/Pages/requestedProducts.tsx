import { useState } from "react";
import { Bar } from "react-chartjs-2";

type Period = "years" | "months" | "quarters" | "weeks";

export default function RequestedProducts({
  months,
  years,
  quarters,
  weeks,
  error,
}) {
  if (error) {
    return <p className="text-center">error occurred, let me know</p>;
  }
  const [period, setPeriod] = useState<Period>("years");

  const labels =
    period === "months"
      ? Object.keys(eval(period)).map((m) => m.substring(0, 3))
      : Object.keys(eval(period));

  const data = Object.values(eval(period));

  return (
    <>
      <div className="col-md-12">
        <h4 className="text-center">
          Last{" "}
          <button
            type="button"
            className={`btn btn-link ${
              period !== "years" && "text-secondary"
            } `}
            onClick={() => setPeriod("years")}
          >
            years
          </button>
          /
          <button
            type="button"
            className={`btn btn-link ${
              period !== "quarters" && "text-secondary"
            }`}
            onClick={() => setPeriod("quarters")}
          >
            quarters
          </button>
          /
          <button
            type="button"
            className={`btn btn-link ${
              period !== "months" && "text-secondary"
            }`}
            onClick={() => setPeriod("months")}
          >
            months
          </button>
          product requests
        </h4>
      </div>
      <Bar
        data={{
          labels,
          datasets: [
            {
              label: `Products requests by last ${period}`,
              data,
            },
          ],
        }}
      />
      <TableBeneathDiag data={eval(period)} />
    </>
  );
}

function TableBeneathDiag({ data }) {
  if (Object.keys(data)[0].includes(" ")) {
    data = Object.entries(data);
  } else {
    data = Object.entries(data).reverse();
  }

  // function formatDate(period: string) {
  //   let m: moment.Moment, unit: moment.unitOfTime.StartOf;
  //   switch (period.length) {
  //     case 4:
  //       [m, unit] = [moment(period), "year"];
  //       break;
  //     case 7:
  //       [m, unit] = [
  //         moment(period.slice(-4)).quarter(+period.slice(1, 2)),
  //         "quarter",
  //       ];
  //       break;
  //     default:
  //       [m, unit] = [moment(period, "MMMM YYYY"), "month"];
  //   }

  //   return `${m.startOf(unit).format("YYYY-MM-DD")}:${m
  //     .endOf(unit)
  //     .format("YYYY-MM-DD")}`;
  // }

  const Column = ({ data }) => (
    <div className="col-md-6">
      <ul className="list-group">
        {data.map(([period, amount], index: number) => (
          <li key={index} className="list-group-item">
            <a
              href={`/requests?period=c&type=withProducts&dates=${globalThis.formatDate(
                period
              )}`}
            >
              {period}: {amount}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );

  return (
    <div className="row">
      <Column data={data.slice(0, 5)} />
      <Column data={data.slice(5, 10)} />
    </div>
  );
}
