import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import {
  SendBtn,
  Sent,
  SentStatus,
  OtherEmails,
  SendToMe,
  EntitySelection,
} from "./Elements";
import { TypeSelector, types } from "./TypeSelector";
import { TypeSelectorContext } from "../requestsToEmail";

type RequestOfDeparmentProps = {
  departments: {};
  remoteURL: string;
};
export default function RequestOfDeparment({
  departments,
  remoteURL,
}: RequestOfDeparmentProps) {
  const { Error, Success, Initial, Sending } = SentStatus;
  const [sent, setSent] = useState<SentStatus>(Initial);
  const type = useContext(TypeSelectorContext).TSState["department"];

  const {
    register,
    handleSubmit,
    setError,
    watch,
    clearErrors,
    formState: { errors },
  } = useForm();

  watch(() => sent && setSent(Initial));
  const onSubmit = ({ department, sendToMe, otherEmails }) => {
    if (otherEmails === "" && !sendToMe) {
      setError(
        "otherEmails",
        {
          type: "noRecipientSpecified",
          message: "no recipient specified",
        },
        {
          shouldFocus: true,
        }
      );
      return;
    }
    setSent(Sending);

    // @ts-expect-error
    const { content: token } = document.head.querySelector(
      'meta[name="csrf-token"]'
    );

    fetch(remoteURL, {
      method: "POST",
      body: JSON.stringify({
        department: department.toLowerCase(),
        sendToMe,
        otherEmails,
        period: "c",
        type,
      }),
      headers: {
        "content-type": "application/json",
        "x-csrf-token": token,
      },
    })
      .then(({ ok }) => setSent(ok ? Success : Error))
      .catch(console.log);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row align-items-center">
          <EntitySelection
            type="department"
            errors={errors.manager}
            register={{
              ...register("department", {
                required: true,
              }),
            }}
            // @ts-expect-error
            options={departments}
          />
          <SendToMe
            tabName="department"
            register={{
              ...register("sendToMe", {
                onChange: () => clearErrors("otherEmails"),
              }),
            }}
          />
        </div>

        <OtherEmails
          errors={errors.otherEmails}
          register={{
            ...register("otherEmails", {
              pattern: {
                value:
                  /^\s*(?:([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})\b\s*)+$/i,
                message: "Please enter valid email/emails",
              },
            }),
          }}
        />
        <TypeSelector tab="department" />
        <div className="d-grid col-1">
          <SendBtn disabled={sent === Sending} />
        </div>
      </form>
      <Sent status={sent} />
    </>
  );
}
