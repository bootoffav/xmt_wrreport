import { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import {
  SendBtn,
  Sent,
  SentStatus,
  OtherEmails,
  SendToMe,
  SendToManager,
  EntitySelection,
} from "./Elements";
import { TypeSelector } from "./TypeSelector";
import { TypeSelectorContext } from "../requestsToEmail";

function sortManagersAlphabetically(
  managers: RequestsOfManagerProps["managers"]
) {
  return managers.sort((a, b) => {
    if (a[1] > b[1]) return 1;
    if (a[1] < b[1]) return -1;
    return 0;
  });
}

type RequestsOfManagerProps = {
  remoteURL: string;
  managers: [string, string][];
};

export default function RequestsOfManager({
  managers,
  remoteURL,
}: RequestsOfManagerProps) {
  const { Error, Success, Initial, Sending } = SentStatus;
  managers = sortManagersAlphabetically(managers);
  const [sent, setSent] = useState<SentStatus>(Initial);
  const type = useContext(TypeSelectorContext).TSState["manager"];

  const {
    register,
    handleSubmit,
    setError,
    watch,
    clearErrors,
    formState: { errors },
  } = useForm();

  watch(() => sent && setSent(Initial));
  const onSubmit = ({ manager, sendToMe, sendToManager, otherEmails }) => {
    if (otherEmails === "" && !sendToMe && !sendToManager) {
      setError(
        "otherEmails",
        {
          type: "noRecipientSpecified",
          message: "no recipient specified",
        },
        {
          shouldFocus: true,
        }
      );
      return;
    }
    setSent(Sending);
    // @ts-ignore
    const { content: token } = document.head.querySelector(
      'meta[name="csrf-token"]'
    );

    fetch(remoteURL, {
      method: "POST",
      body: JSON.stringify({
        manager,
        sendToManager,
        sendToMe,
        otherEmails,
        period: "c",
        type,
      }),
      headers: {
        "content-type": "application/json",
        "x-csrf-token": token,
      },
    })
      .then(({ ok }) => setSent(ok ? Success : Error))
      .catch(console.log);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row align-items-center">
          <EntitySelection
            type="manager"
            errors={errors.manager}
            register={{
              ...register("manager", {
                required: true,
              }),
            }}
            options={managers}
          />

          <SendToMe
            tabName="manager"
            register={{
              ...register("sendToMe", {
                onChange: () => clearErrors("otherEmails"),
              }),
            }}
          />
          <SendToManager
            register={{
              ...register("sendToManager", {
                onChange: () => clearErrors("otherEmails"),
              }),
            }}
          />
        </div>

        <OtherEmails
          errors={errors.otherEmails}
          register={{
            ...register("otherEmails", {
              pattern: {
                value:
                  /^\s*(?:([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})\b\s*)+$/i,
                message: "Please enter valid email/emails",
              },
            }),
          }}
        />
        <TypeSelector tab="manager" />
        <div className="d-grid col-1">
          <SendBtn disabled={sent === Sending} />
        </div>
      </form>
      <Sent status={sent} />
    </>
  );
}
