export enum SentStatus {
  Initial,
  Sending,
  Error,
  Success,
}

export const Sent = ({ status }: { status: SentStatus }) => {
  switch (status) {
    case SentStatus.Initial:
    case SentStatus.Sending:
      return <></>;
    case SentStatus.Error:
      return (
        <div className="text-danger text-center">
          Something was wrong, retry send
        </div>
      );
    case SentStatus.Success:
      return <div className="text-success text-center">Successfully sent!</div>;
  }
};

export const SendBtn = ({ disabled }: { disabled: boolean }) => {
  return (
    <button
      className="btn btn-outline-primary"
      type="submit"
      disabled={disabled}
    >
      {disabled ? (
        <span
          className="spinner-border spinner-border-sm"
          aria-hidden="true"
        ></span>
      ) : (
        ""
      )}
      <span role="status"> Send</span>
    </button>
  );
};

export const OtherEmails = ({ register, errors }) => {
  return (
    <div className="form-floating my-2">
      <textarea className="form-control" id="otherEmails" {...register} />
      <label htmlFor="otherEmails">Other emails (single space separated)</label>
      {errors && <p className="text-danger mt-1">{`${errors.message}`}</p>}
    </div>
  );
};

export const SendToManager = ({ register }) => {
  return (
    <div className="col form-check">
      <input
        type="checkbox"
        id="sendToManager"
        className="form-check-input"
        placeholder="Send to manager"
        {...register}
      />
      <label className="form-check-label" htmlFor="sendToManager">
        Send to manager
      </label>
    </div>
  );
};

type SendToMeProps = {
  register: {};
  tabName: "manager" | "department";
};
export const SendToMe = ({ register, tabName }: SendToMeProps) => {
  return (
    <div className="col offset-1 form-check">
      <input
        type="checkbox"
        id={`sendToMe-${tabName}`}
        className="form-check-input"
        placeholder="Send to me"
        defaultChecked
        {...register}
      />
      <label className="form-check-label" htmlFor={`sendToMe-${tabName}`}>
        Send to me
      </label>
    </div>
  );
};

type EntitySelectionProps = {
  type: "manager" | "department";
  register: {};
  options: any[];
  errors: any;
};
export const EntitySelection = ({
  register,
  options,
  errors,
  type,
}: EntitySelectionProps) => {
  const managerOptionBuilder = () =>
    options.map(([id, name]) => (
      <option value={id} key={id}>
        {name}
      </option>
    ));

  const depOptionBuilder = () =>
    Object.entries(options).map(([dep, desc]) => (
      <option value={dep} key={dep}>
        {desc}
      </option>
    ));

  return (
    <div className="col-6">
      <label htmlFor={type}>Choose {type}</label>
      <select id={type} className="form-select mb-2" {...register}>
        <option />
        {type === "manager" ? managerOptionBuilder() : depOptionBuilder()}
      </select>
      {errors && <p className="text-danger mt-1">{type} is not chosen</p>}
    </div>
  );
};
