export default function Tabs({ children }) {
  const tabNames = [
    "requests-of-manager",
    "requests-of-department",
    "requests-for-period",
  ];

  const tabs = tabNames.map((tabName, i) => (
    <li key={tabName} className="nav-item" role="presentation">
      <button
        className={`nav-link ${i === 0 ? "active" : ""}`}
        id={`${tabName}-tab`}
        data-bs-toggle="tab"
        data-bs-target={`#${tabName}-pane`}
        type="button"
        role="tab"
        aria-controls={`${tabName}-pane`}
        aria-selected="true"
      >
        {tabName.charAt(0).toUpperCase() +
          tabName.slice(1).replaceAll("-", " ")}
      </button>
    </li>
  ));

  const tabContents = tabNames.map((tabName, i) => (
    <div
      key={tabName}
      className={`tab-pane fade show ${i === 0 ? "active" : ""}`}
      id={`${tabName}-pane`}
      role="tabpanel"
      aria-labelledby={`${tabName}-tab`}
      tabIndex={0}
    >
      {children[i]}
    </div>
  ));

  return (
    <>
      <ul className="nav nav-tabs" id="requestsToEmailTab" role="tablist">
        {tabs}
      </ul>
      <div className="tab-content mt-2" id="requestsToEmailContent">
        {tabContents}
      </div>
    </>
  );
}
