import { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { SendBtn, Sent, SentStatus } from "./Elements";
import { TypeSelectorContext } from "../requestsToEmail";
import { TypeSelector } from "./TypeSelector";

export default function RequestsForPeriod({ recipients }) {
  const { Initial, Error, Success, Sending } = SentStatus;
  const [sent, setSent] = useState<SentStatus>(Initial);
  const type = useContext(TypeSelectorContext).TSState["period"];

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      recipients,
      otherEmails: "",
    },
  });

  watch(() => sent && setSent(Initial));
  const onSubmit = ({ recipients, otherEmails }) => {
    setSent(Sending);
    const { content: token } = document.head.querySelector(
      'meta[name="csrf-token"]'
    ) as unknown as { content: string };

    recipients = [
      ...recipients,
      ...(otherEmails ? otherEmails.split(" ") : []),
    ];

    fetch(globalThis.location.origin + "/requestsOfPeriodToEmail", {
      method: "POST",
      body: JSON.stringify({
        recipients,
        period: "c",
        type,
      }),
      headers: {
        "content-type": "application/json",
        "x-csrf-token": token,
      },
    })
      .then(({ ok }) => setSent(ok ? Success : Error))
      .catch(console.log);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label htmlFor="recipients" className="mt-2">
          Recipients (all selected by default)
        </label>
        <select
          {...register("recipients", { required: true })}
          id="recipients"
          size={14}
          className="form-select"
          multiple
          aria-label="Multiple recipients select"
        >
          {recipients.map((rcpt) => (
            <option value={rcpt} key={rcpt}>
              {rcpt}
            </option>
          ))}
        </select>

        <div className="form-floating my-2">
          <textarea
            className="form-control"
            id="otherEmails"
            {...register("otherEmails", {
              pattern: {
                value:
                  /^\s*(?:([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})\b\s*)+$/i,
                message: "Please enter valid email/emails",
              },
            })}
          />
          <label htmlFor="otherEmails">
            Other emails (single space separated)
          </label>
          {errors.otherEmails && (
            <p className="text-danger mt-1">{`${errors.otherEmails.message}`}</p>
          )}
        </div>

        <TypeSelector tab="period" />
        <div className="d-grid col-1 my-2">
          <SendBtn disabled={sent === Sending} />
        </div>
      </form>
      <Sent status={sent} />
    </>
  );
}
