import { useContext } from "react";
import { TypeSelectorContext } from "../requestsToEmail";

export const types = {
  email: "Email",
  wr: "Web-Request",
  p: "Processed",
  np: "Not processed",
};

export function TypeSelector({
  tab,
}: {
  tab: "department" | "manager" | "period";
}) {
  const { TSState, updateTSState } = useContext(TypeSelectorContext);
  const state = TSState[tab];

  return (
    <div className="d-flex justify-content-between pb-2">
      {Object.entries(types).map(([type, desc]) => (
        <div key={type} className="form-check form-check-inline">
          <input
            className="form-check-input typeSelectorCheckbox"
            name={`${tab}-radio`}
            type="checkbox"
            id={`${tab}-${type}`}
            value={type}
            checked={state.includes(type as keyof typeof types)}
            onChange={updateTSState}
          />
          <label className="form-check-label" htmlFor={`${tab}-${type}`}>
            {desc}
          </label>
        </div>
      ))}
    </div>
  );
}
