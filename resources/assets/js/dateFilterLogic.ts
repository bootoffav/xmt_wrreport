import $ from "jquery";
import "daterangepicker";
import type { Moment } from "moment";

$('input[name="datefilter"]').daterangepicker({
  maxDate: globalThis.moment().endOf("month"),
  minDate: globalThis.moment("2015-01-01"),
  showDropdowns: true,
  autoUpdateInput: false,
  locale: {
    cancelLabel: "Clear",
  },
});
$('input[name="datefilter"]').on("apply.daterangepicker", function (_, picker) {
  $(this).val(
    picker.startDate.format("YYYY-MM-DD") +
      ":" +
      picker.endDate.format("YYYY-MM-DD")
  );
});

$('input[name="datefilter"]').on("cancel.daterangepicker", function () {
  $(this).val("");
});

$(
  'input[id="lastYear"],\
  input[id="thisYear"],\
  input[id="thisMonth"],\
  input[id="lastMonth"],\
  input[id="lastQuarter"],\
  input[id="thisQuarter"],\
  input[id="lastWeek"],\
  input[id="thisWeek"]'
).on("click", ({ currentTarget }: Event) => {
  const unit = (currentTarget as HTMLInputElement).id
    .substring(4)
    .trim()
    .toLowerCase() as moment.unitOfTime.Base; // strip 'last|this';
  const date = (currentTarget as HTMLInputElement).id.startsWith("last")
    ? globalThis.moment().subtract(1, unit)
    : globalThis.moment();

  const start = date.startOf(unit).format("YYYY-MM-DD");
  const end = date.endOf(unit).format("YYYY-MM-DD");
  $('input[name="datefilter"]').val(`${start}:${end}`);
});

$('button[id="reset"]').on("click", (e) => {
  $('input[name="datefilter"]').val("");
  e.preventDefault();
});
