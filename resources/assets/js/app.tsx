import("bootstrap");
import { createInertiaApp } from "@inertiajs/react";
import { createRoot } from "react-dom/client";
const moment = await import("moment");
globalThis.moment = moment.default;

import("./chartLogic");
import("./dateFilterLogic");

import("./Pages/Summary").then(({ show, showPeriod, showRequestSources }) => {
  globalThis.show = show;
  globalThis.showPeriod = showPeriod;
  globalThis.showRequestSources = showRequestSources;
});

globalThis.changeShowItemsPerPage = function (option: any) {
  // @ts-ignore
  const { content: token } = document.head.querySelector(
    'meta[name="csrf-token"]'
  );
  fetch(`${globalThis.location.origin}/options`, {
    method: "POST",
    body: JSON.stringify({
      showItemsPerPage: option.value,
    }),
    headers: {
      "content-type": "application/json",
      "x-csrf-token": token,
    },
  })
    .then(() => {
      location.reload();
    })
    .catch(console.log);
};

globalThis.formatDate = function (period: string) {
  let m: moment.Moment, unit: moment.unitOfTime.StartOf;
  switch (period.length) {
    case 4:
      [m, unit] = [globalThis.moment(period), "year"];
      break;
    case 7:
      [m, unit] = [
        globalThis.moment(period.slice(-4)).quarter(+period.slice(1, 2)),
        "quarter",
      ];
      break;
    default:
      [m, unit] = [globalThis.moment(period, "MMMM YYYY"), "month"];
  }

  return `${m.startOf(unit).format("YYYY-MM-DD")}:${m
    .endOf(unit)
    .format("YYYY-MM-DD")}`;
};

if (["/requestsToEmail", "/requestedProducts"].includes(location.pathname)) {
  createInertiaApp({
    resolve: (name) => {
      const pages = import.meta.glob("./Pages/**/*.tsx");
      return pages[`./Pages/${name}.tsx`]();
    },
    setup({ el, App, props }) {
      createRoot(el).render(<App {...props} />);
    },
  });
}
