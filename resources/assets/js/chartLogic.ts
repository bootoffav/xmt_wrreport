import Chart from "chart.js/auto";

Chart.defaults.font.size = 16;
// Chart.overrides.bar = {
// scales: {

// yAxes: [
//   {
//     ticks: {
//       precision: 0,
//     },
//   },
// ],
// },
// };

globalThis.Chart = Chart;
globalThis.barChartOptions = {
  scales: {
    y: {
      beginAtZero: true,
    },
  },
};

globalThis.chartOptions = {
  plugins: {
    legend: {
      display: false,
    },
  },
};

globalThis.getChartOptions = (label: string) => {
  let options = globalThis.chartOptions;

  switch (label) {
    case "Requests of last years":
      options = {
        ...options,
        ...globalThis.barChartOptions,
      };
      break;
    case "Sale requests of last years":
    case "Sale requests of last 12 months":
      options = {
        plugins: {
          legend: {
            display: false,
          },
          tooltip: {
            callbacks: {
              label: ({ raw }) =>
                new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: "USD",
                  maximumFractionDigits: 0,
                }).format(raw),
            },
          },
        },
        scales: {
          y: {
            beginAtZero: true,
            ticks: {
              callback: (v) =>
                new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: "USD",
                  maximumFractionDigits: 0,
                }).format(v),
            },
          },
        },
      };
  }

  return options;
};

globalThis.location.pathname === "/department/all" &&
  [
    "other",
    "bangladesh",
    "brazil",
    "china",
    "hu_sk_cz",
    "italy",
    "kazakhstan",
    "lithuania",
    "poland",
    "romania",
    "russia",
    "spain",
    "turkey",
    "usa",
  ].forEach((tab) => !!globalThis[`${tab}Fn`] && globalThis[`${tab}Fn`]());

globalThis.location.pathname === "/" &&
  [
    "websiteyears",
    "websitemonths",
    "emailyears",
    "emailmonths",
    "departmentsAllocation",
    "yearsSale",
    "quartersSale",
    "monthsSale",
    "years",
    "quarters",
    "months",
    "country",
  ].forEach((id) => globalThis[`${id}Fn`]());
