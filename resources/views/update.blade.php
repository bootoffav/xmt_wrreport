@extends('app')

@section('content')
  <p class="fs-5 text-center">
    {{ session('noData') ? 
      "Report has no data, it's going to collect it,"
      :
      'Updates have been requested,'
    }} system will send you an email when finished.
  </p>
@endsection