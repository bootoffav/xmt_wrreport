@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">{{ $subject }} Requests of every Year</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($eachYear as $year => $requests)
                    <tr>
                    <td>
                        <a href="{{ $subject === 'Sale' ? '/sale' : ''}}/year/{{ $year }}">{{ $year }}</a>
                    </td>
                    <td>
                        {{ count($requests) }}
                    </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection