<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>
        <a href="{{ $dep }}">{{ formatDepName($dep) }}</a>
      </th>
      <th class="quantity">{{ $total }}</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($requests as $userId => $amount)
    <tr>
      <td class="">
        <a href="/user/{{ $userId }}">{{ $realName_id[$userId]['NAME'] }}</a>
      </td>
      <td class="quantity">{{ $amount }}</td>
    </tr>
  @endforeach
  </tbody>
</table>
