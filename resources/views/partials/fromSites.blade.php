<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
<thead>
    <tr>
        <th colspan="2" class="mdl-data-table__cell--non-numeric">Requests from sites</th>
    </tr>
</thead>
    <tbody>
        @foreach($from_sites as $site => $requests)
        <tr>
            <td class="mdl-data-table__cell--non-numeric"><a href="/{{ $site }}">{{ $site }}</a></td>
            <td>{{ count($requests) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>