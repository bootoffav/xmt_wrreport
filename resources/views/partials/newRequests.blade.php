<div id="newRequests" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <span>New Web requests</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <table class="table table-bordered table-cover">
                <thead>
                    <tr>
                    <th class="">Web Request</th>
                    <th class="">Country</th>
                    <th class="">Manager</th>
                    <th class="">Brand</th>
                    <th class="">Products</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($newWebRequests as $r)
                    <tr class="request">
                    <td><a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/" target="_blank">{{ $r->TITLE}}</a></td>
                    <td><a href="/country/{{ $r->country }}">{{ $r->country }}</a></td>
                    <td><a href="/user/{{ $r->RESPONSIBLE_ID }}">{{ $realName_id[$r->RESPONSIBLE_ID]['NAME'] }}</a></td>
                    <td width="12%">
                    @unless (is_null($r->brands))
                        @foreach ($r->brands as $brand)
                            <p><a href="/brand/{{ $brand }}">{{ $brand }}</a></p>
                        @endforeach
                    @endunless
                    </td>
                    <td>
                    @unless (is_null($r->products))
                        @foreach ($r->products as $product)
                            <p><a href="/product/{{ $product }}">{{ $product }}</a></p>
                        @endforeach
                    @endunless
                    </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>