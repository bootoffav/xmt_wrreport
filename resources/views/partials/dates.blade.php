<div id="dates" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('dates') }}" method="POST">
                @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="datefilter">Select date range:</label>
                        <input
                          type="text"
                          autocomplete="off"
                          placeholder="blank resets dates"
                          name="datefilter"
                          id="datefilter"
                          value="{{ old('datefilter') ?? session('datefilter') }}"
                          class="form-control"
                          data-bs-toggle="tooltip"
                          data-bs-placement="top"
                          title="format YYYY-MM-DD:YYYY-MM-DD"
                        />
                        @error('datefilter')
                          <p class="text-danger">{{ $message }}</p>
                        @enderror
                </div>
            </div>
            <div class="d-flex justify-content-start mb-3 mx-3 gap-2">
                <div class="btn-group" role="group">
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="lastYear" autocomplete="off">
                    <label class="btn btn-outline-primary" for="lastYear">Last Y</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="thisYear" autocomplete="off">
                    <label class="btn btn-outline-primary" for="thisYear">This Y</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="lastQuarter" autocomplete="off">
                    <label class="btn btn-outline-primary" for="lastQuarter">Last Q</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="thisQuarter" autocomplete="off">
                    <label class="btn btn-outline-primary" for="thisQuarter">This Q</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="lastMonth" autocomplete="off">
                    <label class="btn btn-outline-primary" for="lastMonth">Last M</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="thisMonth" autocomplete="off">
                    <label class="btn btn-outline-primary" for="thisMonth">This M</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="lastWeek" autocomplete="off">
                    <label class="btn btn-outline-primary" for="lastWeek">Last W</label>
                    <input type="radio" class="btn-check" name="datePeriodRadio" id="thisWeek" autocomplete="off">
                    <label class="btn btn-outline-primary" for="thisWeek">This W</label>
                  </div>
                <button type="submit" class="btn btn-sm btn-secondary ms-auto" id="reset">reset</button>
                <button type="submit" class="btn btn-sm btn-success">Apply</button>
            </div>
            </form>
        </div>
    </div>
</div>

