@unless (is_null($r->revenue))
    @foreach ($r->revenue as $revenue)
        @if ($param === 'amount')
            {{ (new NumberFormatter('en_US', NumberFormatter::CURRENCY))->formatCurrency($revenue[$param], 'USD') }}
        @else
            {{ $revenue[$param] }}
        @endif
    @endforeach
@endunless