<div class="row">
    <div class="col-md-12">
        <h4 class="text-center">
            Last
            <a type="button" class="btn btn-link active-a" data-area="yearsSale-chart-area" onclick="show(event)">years</a> /
            <a type="button" class="btn btn-link active-a" data-area="quartersSale-chart-area" onclick="show(event)">quarters</a> /
            <a type="button" class="btn btn-link" data-area="monthsSale-chart-area" onclick="show(event)">months</a>
            sale requests
        </h4>
        <h5 class="text-center">all:
            <a type="button" href="/sale/year/all"><small>years</small></a>
            /
            <a type="button" href="/sale/month/all"><small>months</small></a>
        </h5>
    </div>
    <div id="yearsSale-chart-area" class="row">
      <div class="col-md-12">
          <canvas id="yearsSale-chart"></canvas>
          @include('partials.chart', [
              'requests' => $eachYearSaleSum,
              'chartholder' => 'yearsSale',
              'type' => 'bar',
              'label' => 'Sale requests of last years'
              ])
      </div>
      <div class="col-md-12">
        <div class="row">
            @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                    @foreach ($eachYearSale as $year => $yearAmount)
                    @php
                        $splited = explode(' ', $yearAmount);
                    @endphp
                        <li class="list-group-item"><a href="/sale/year/{{ $year }}">{{ $year }}: {{ 
                            $splited[0]
                            . ' ('
                            . (new \NumberFormatter('en_US', \NumberFormatter::CURRENCY))->formatCurrency($splited[1], 'USD')
                            . ')'
                            }}</a></li>
                        @break($loop->index == 3)
                    @endforeach
                </ul>
            </div>
                @php
                    $eachYearSale = array_slice($eachYearSale, 4, 6, true);
                @endphp
            @endfor
        </div>
      </div>
    </div>

    <div id="quartersSale-chart-area" class="row" style="display: none;">
      <div class="col-md-12">
          <canvas id="quartersSale-chart"></canvas>
          @include('partials.chart', [
              'requests' => $eachQuarterSaleSum,
              'chartholder' => 'quartersSale',
              'type' => 'bar',
              'label' => 'Sale requests of last quarters'
              ])
      </div>
      <div class="col-md-12">
        <div class="row">
            @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                    @foreach ($eachQuarterSale as $quarter => $quarterAmount)
                    @php
                        [$q, $year] = explode(' ', $quarter);
                        [$amount, $sum] = explode(' ', $quarterAmount);
                        $qM = [
                            'Q1' => ['01-01', '03-31'],
                            'Q2' => ['04-01', '06-30'],
                            'Q3' => ['07-01', '09-30'],
                            'Q4' => ['10-01', '12-31']
                        ];
                        $date_format = "$year-{$qM[$q][0]}:$year-{$qM[$q][1]}"; //for links
                    @endphp
                        <li class="list-group-item"><a href="/requests?period=c&dates={{ $date_format }}">{{ $quarter }}: {{
                            $amount
                            . ' ('
                            . (new \NumberFormatter('en_US', \NumberFormatter::CURRENCY))->formatCurrency($sum, 'USD')
                            . ')'
                            }}</a></li>
                        @break($loop->index == 3)
                    @endforeach
                </ul>
            </div>
                @php
                    $eachQuarterSale = array_slice($eachQuarterSale, 4, 6, true);
                @endphp
            @endfor
        </div>
      </div>
    </div>
    
    
        <div id="monthsSale-chart-area" style="display: none;">
    <div class="col-md-12">
          <canvas id="monthsSale-chart"></canvas>
          @include('partials.chart', [
              'requests' => $eachMonthSaleSum,
              'chartholder' => 'monthsSale',
              'type' => 'bar',
              'label' => 'Sale requests of last 12 months'
              ])
      </div>
      
      <div class="row">
        @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                @for ($i = 1; count($eachMonthSale); $i++)
                    @php
                        $month = key($eachMonthSale);
                        $monthAmount = array_shift($eachMonthSale);
                        $splited = explode(' ', $monthAmount);
                        $date_format = explode(' ', $month); //for links
                    @endphp
                    <li class="list-group-item"><a href="/sale/{{ $date_format[1] }}-{{ $date_format[0] }}">{{ $month }}: {{ 
                        $splited[0]
                        . ' ('
                        . (new \NumberFormatter('en_US', \NumberFormatter::CURRENCY))->formatCurrency($splited[1], 'USD')
                        . ')';
                        }}</a>
                    </li>
                    @break($i == 6)
                @endfor
                </ul>
            </div>
            @endfor
      </div>
    </div>
</div> <!-- end row -->


