@php
$webRequestsCurPeriodInPercents = round(count($rpc->curSummary['wr']) * 100 / (count($rpc->requests) ?: 1));
$webRequestsPrevPeriodInPercents = round(count($rpc->prevSummary['wr']) * 100 / ($rpc->prevRequestsAmount ?: 1));
$processedCurPeriodInPercents = round(count($rpc->curSummary['p']) * 100 / (count($rpc->requests) ?: 1));
$processedPrevPeriodInPercents = round(count($rpc->prevSummary['p']) * 100 / ($rpc->prevRequestsAmount ?: 1));
$withProductsCurPeriodInPercents = round(count($rpc->curSummary['withProducts']) * 100 / (count($rpc->curSummary['wr']) ?: 1));
$withProductsPrevPeriodInPercents = round(count($rpc->prevSummary['withProducts']) * 100 / (count($rpc->prevSummary['wr']) ?: 1)); 
@endphp

<div class="row">
  <div class="col">
    <table class="table borderless">
      <thead>
        <tr class="table-secondary">
          <th class="text-uppercase fs-6"> {{ $rpc->curPeriod }}:</th>
          <th class="text-uppercase fs-6">{{ $rpc->prevPeriod }}:</th>
          <th class="text-uppercase fs-6">Dynamics:</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <a
              class="summaryLink"
              href={{ formFilteredRequestLink(['c', 'wr'], $targetView, $dep ?? null, $path ?? null )}}
              >Web-Requests: {{ count($rpc->curSummary['wr']) }} ({{ $webRequestsCurPeriodInPercents }}%)</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'wr'], $targetView, $dep ?? null, $path ?? null) }}
              >Web-Requests: {{ count($rpc->prevSummary['wr']) }} ({{ $webRequestsPrevPeriodInPercents }}%)</a>
            </td>
            <td
            @class([ 'text-danger' => $rpc->dynamics['wr']['deviation_int'] < 0 ])
            >{{ $rpc->dynamics['wr']['deviation_formatted'] }}</td>
          </tr>
          <tr>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['c', 'email'], $targetView, $dep ?? null, $path ?? null) }}
              >Email: {{ count($rpc->curSummary['email']) }} ({{ 100 - $webRequestsCurPeriodInPercents }}%)</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'email'], $targetView, $dep ?? null, $path ?? null) }}
              >Email: {{ count($rpc->prevSummary['email']) }} ({{ 100 - $webRequestsPrevPeriodInPercents }}%)</a>
            </td>
            <td
            @class(['text-danger' => $rpc->dynamics['email']['deviation_int'] < 0])
            >{{ $rpc->dynamics['email']['deviation_formatted'] }}</td>
          </tr>
          <tr>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['c', 'all'], $targetView, $dep ?? null, $path ?? null) }}
              >Total: {{ count($rpc->requests) }}</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'all'], $targetView, $dep ?? null, $path ?? null) }}
              >Total: {{ $rpc->prevRequestsAmount }}</a>
            </td>
            <td
            @class([ 'text-danger' => $rpc->dynamics['all']['deviation_int'] < 0 ])
            >{{ $rpc->dynamics['all']['deviation_formatted'] }}</td>
          </tr>
          <tr>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['c', 'p'], $targetView, $dep ?? null, $path ?? null) }}
              >Processed: {{ count($rpc->curSummary['p']) }} ({{ $processedCurPeriodInPercents }}%)</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'p'], $targetView, $dep ?? null, $path ?? null) }}
              >Processed: {{ count($rpc->prevSummary['p']) }} ({{ $processedPrevPeriodInPercents }}%)</a>
            </td>
            <td
            @class([ 'text-danger' => $rpc->dynamics['p']['deviation_int'] < 0])
            >{{ $rpc->dynamics['p']['deviation_formatted'] }}</td>
          </tr>
          <tr>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['c', 'np'], $targetView, $dep ?? null, $path ?? null) }}
              >Not processed: {{ count($rpc->curSummary['np']) }} ({{ 100 - $processedCurPeriodInPercents }}%)</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'np'], $targetView, $dep ?? null, $path ?? null) }}
            >Not processed: {{ count($rpc->prevSummary['np']) }} ({{ 100 - $processedPrevPeriodInPercents }}%)</a>
            </td>
            <td
              @class([ 'text-danger' => $rpc->dynamics['np']['deviation_int'] < 0])
            >{{ $rpc->dynamics['np']['deviation_formatted'] }}</td>
          </tr>
          <tr>
            <td>
              <a class="summaryLink"
               href={{ formFilteredRequestLink(['c', 'withProducts'], $targetView, $dep ?? null, $path ?? null) }}
              >Products: {{ count($rpc->curSummary['withProducts']) }} ({{ $withProductsCurPeriodInPercents }}%)</a>
            </td>
            <td>
              <a class="summaryLink"
              href={{ formFilteredRequestLink(['p', 'withProducts'], $targetView, $dep ?? null, $path ?? null) }}
              >Products: {{ count($rpc->prevSummary['withProducts']) }} ({{ $withProductsPrevPeriodInPercents }}%)</a>
            </td>
            <td
              @class([ 'text-danger' => $rpc->dynamics['withProducts']['deviation_int'] < 0])
            >{{ $rpc->dynamics['withProducts']['deviation_formatted'] }}</td>
        </tr>
        @if (isset($isAnnualReport) and $isAnnualReport)
          <tr>
            <td>Sales: {{ (new NumberFormatter('en_US', NumberFormatter::CURRENCY))->formatCurrency($rpc->curSummary['sales'], 'USD') }} ({{ count($rpc->curSummary['saleReqs'])}} clients)</td>
            <td>Sales: {{ (new NumberFormatter('en_US', NumberFormatter::CURRENCY))->formatCurrency($rpc->prevSummary['sales'], 'USD') }} ({{ count($rpc->prevSummary['saleReqs'])}} clients)</td>
            <td
              @class(['text-danger' => $rpc->dynamics['sales']['deviation_int'] < 0])
            >{{ $rpc->dynamics['sales']['deviation_formatted'] }}</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>