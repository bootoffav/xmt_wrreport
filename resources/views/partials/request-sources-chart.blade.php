<div class="row">
  <div class="col">
    <h4 class="text-center">
        Request sources
        <a type="button" class="btn btn-link active-a" data-area="website-chart-area" onclick="showRequestSources(event)">web-site</a> /
        <a type="button" class="btn btn-link" data-area="email-chart-area" onclick="showRequestSources(event)">email</a> 
    </h4>
    <h5 class="text-center">Period:
        <a type="button" class="btn btn-link active-a" id="years-period-link" onclick="showPeriod(event, 'years', 'months')"><small>years</small></a>
        /
        <a type="button" class="btn btn-link" id="months-period-link" onclick="showPeriod(event, 'months', 'years')"><small>months</small></a>
    </h5>
  </div>
</div>

<div id="website-chart-area">
      <div class="col">
        <canvas id="websiteyears-chart"></canvas>
        @include('partials.chart', [
          'requests' => array_map(fn($v): int => count($v), $websiteRequestsByYears),
          'chartholder' => 'websiteyears',
          'type' => 'bar',
          'label' => 'Requests from website'
        ])
        <canvas id="websitemonths-chart" style="display: none;"></canvas>
        @include('partials.chart', [
          'requests' => array_map(fn($v): int => count($v), $websiteRequestsByLastMonths),
          'chartholder' => 'websitemonths',
          'type' => 'bar',
          'label' => 'Requests from website'
        ])
      </div>

    <div id="website-years-period">
      <div class="row">
        @for ($k = 0; $k !== 2; $k++)
        <div class="col">
          <ul class="list-group">
              @foreach ($websiteRequestsByYears as $year => $requests)
                  <li class="list-group-item"><a href="#">{{ $year }}: {{ count($requests) }}</a></li>
                  @break($loop->index == 3)
              @endforeach
          </ul>
        </div>
          @php
              $websiteRequestsByYears = array_slice($websiteRequestsByYears, 4, 6, true);
          @endphp
        @endfor
      </div>
    </div>

    <div id="website-months-period" style="display: none;">
      <div class="row">
        <div class="col">
          <ul class="list-group">
          @foreach (array_slice($websiteRequestsByLastMonths, 0, 6) as $data => $reqs)
            <li class="list-group-item">{{ $data }}: {{ count($reqs) }}</li>
          @endforeach
          </ul>
        </div>
        <div class="col">
          <ul class="list-group">
          @foreach (array_slice($websiteRequestsByLastMonths, 6) as $data => $reqs)
            <li class="list-group-item">{{ $data }}: {{ count($reqs) }}</li>
          @endforeach
          </ul>
        </div>
      </div>
    </div>


</div> <!-- end website-chart-area -->



  <div id="email-chart-area" style="display: none;">
    <div class="col">
      <canvas id="emailyears-chart"></canvas>
      @include('partials.chart', [
        'requests' => array_map(fn($v): int => count($v), $emailRequestsByYears),
        'chartholder' => 'emailyears',
        'type' => 'bar',
        'label' => 'Email requests of last years'
      ])
      <canvas id="emailmonths-chart" style="display: none;"></canvas>
      @include('partials.chart', [
        'requests' => array_map(fn($v): int => count($v), $emailRequestsByLastMonths),
        'chartholder' => 'emailmonths',
        'type' => 'bar',
        'label' => 'Email requests of last 12 months'
      ])
    </div>




    <div id="email-years-period">
      <div class="row">
        @for ($k = 0; $k !== 2; $k++)
        <div class="col">
          <ul class="list-group">
            @foreach ($emailRequestsByYears as $year => $requests)
              <li class="list-group-item"><a href="#">{{ $year }}: {{ count($requests) }}</a></li>
              @break($loop->index == 3)
            @endforeach
          </ul>
        </div>
          @php
            $emailRequestsByYears = array_slice($emailRequestsByYears, 4, 6, true);
          @endphp
        @endfor
      </div>
    </div>



    <div id="email-months-period" style="display: none;">
      <div class="row">
        <div class="col">
          <ul class="list-group">
          @foreach (array_slice($emailRequestsByLastMonths, 0, 6) as $data => $reqs)
            <li class="list-group-item">{{ $data }}: {{ count($reqs) }}</li>
          @endforeach
          </ul>
        </div>
        <div class="col">
          <ul class="list-group">
          @foreach (array_slice($emailRequestsByLastMonths, 6) as $data => $reqs)
            <li class="list-group-item">{{ $data }}: {{ count($reqs) }}</li>
          @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>


