<div class="row">
    <div class="col-md-12">
        <h4 class="text-center">
            Last
            <a type="button" class="btn btn-link active-a" data-area="years-chart-area" onclick="show(event)">years</a> /
            <a type="button" class="btn btn-link active-a" data-area="quarters-chart-area" onclick="show(event)">quarters</a> /
            <a type="button" class="btn btn-link" data-area="months-chart-area" onclick="show(event)">months</a>
            requests
        </h4>
        <h5 class="text-center">all:
            <a type="button" href="/year/all"><small>years</small></a>
            /
            <a type="button" href="/month/all"><small>months</small></a>
        </h5>
    </div>

    {{-- years-area --}}
    <div id="years-chart-area" class="row">
      <div class="col-md-12">
          <canvas id="years-chart"></canvas>
          @include('partials.chart', [
              'requests' => $eachYear,
              'chartholder' => 'years',
              'type' => 'bar',
              'label' => 'Requests of last years'
              ])
      </div>
      <div class="col-md-12">
        <div class="row">
            @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                    @foreach ($eachYear as $year => $requestAmount)
                        <li class="list-group-item"><a href="/year/{{ $year }}">{{ $year }}: {{ $requestAmount }}</a></li>
                        @break($loop->index == 3)
                    @endforeach
                </ul>
            </div>
                @php
                    $eachYear = array_slice($eachYear, 4, 6, true);
                @endphp
            @endfor
        </div>
      </div>
    </div>

    
    {{-- quarters-area --}}
    <div id="quarters-chart-area" style="display: none;">
      <div class="col-md-12">
        <canvas id="quarters-chart"></canvas>
        @include('partials.chart', [
            'requests' => $eachQuarter,
            'chartholder' => 'quarters',
            'type' => 'bar',
            'label' => 'Requests of last 12 quarters'
            ])
      </div>
      
      <div class="row">
        @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                @for ($i = 1; count($eachQuarter); $i++)
                    @php
                        $qM = [
                            'Q1' => ['01-01', '03-31'],
                            'Q2' => ['04-01', '06-30'],
                            'Q3' => ['07-01', '09-30'],
                            'Q4' => ['10-01', '12-31']
                        ];
                        [$q, $year] = explode(' ', key($eachQuarter));
                        $qAmount = array_shift($eachQuarter);
                        $date_format = "$year-{$qM[$q][0]}:$year-{$qM[$q][1]}"; //for links
                    @endphp
                    <li class="list-group-item">
                        <a href="/requests?period=c&dates={{ $date_format }}">
                            {{$q . ' ' . $year}}: {{$qAmount}}
                        </a>
                    </li>
                    @break($i == 6)
                @endfor
                </ul>
            </div>
            @endfor
      </div>
    </div>

    {{-- months-area --}}
    <div id="months-chart-area" style="display: none;">
      <div class="col-md-12">
        <canvas id="months-chart"></canvas>
        @include('partials.chart', [
            'requests' => $eachMonth,
            'chartholder' => 'months',
            'type' => 'bar',
            'label' => 'Requests of last 12 months'
            ])
      </div>
      
      <div class="row">
        @for ($k = 0; $k !== 2; $k++)
            <div class="col">
                <ul class="list-group">
                @for ($i = 1; count($eachMonth); $i++)
                    @php
                        $month = key($eachMonth);
                        $monthAmount = array_shift($eachMonth);
                        $date_format = explode(' ', $month); //for links
                    @endphp
                    <li class="list-group-item"><a href="/month/{{ $date_format[1] }}-{{ $date_format[0] }}">{{ $month }}: {{ $monthAmount }}</a></li>
                    @break($i == 6)
                @endfor
                </ul>
            </div>
            @endfor
      </div>
    </div>
</div> <!-- end row -->


