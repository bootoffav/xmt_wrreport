<section class="mdl-layout__tab-panel" id="{{ $item }}">
    <div class="page-content">
        <div class="row">
            <div class="col-md-3">
                <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                    <tr>
                    @php
                        $total = 0;
                        foreach ($requests as $reqs) {
                            $total += count($reqs);
                        }
                    @endphp
                        <th class="mdl-data-table__cell--non-numeric">Web Requests by {{ $item }}</th>
                        <th class="quantity">{{ $total }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($requests as $position => $reqs)
                        @if (count($reqs) === 0) @continue @endif
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">
                                <a href="{{ $item }}/{{ $position }}">{{ $position }}</a>
                            </td>
                            <td class="quantity">{{ count($reqs) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <canvas id="{{ $item }}Chart"></canvas>
                @include('partials.drawChart', [
                    'chartPlaceholder' => $item,
                    'requests' => $requests,
                    'options' => $options
                ])
            </div>
        </div>
    </div>
</section>