<html>
    <head>
        <style>
          {!! Illuminate\Support\Facades\Vite::content('resources/assets/sass/app.scss') !!}
        </style>
      </head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <h4 class="text-center">WRReport cache has been updated, see updates: <a href="{{ config('app.url') }}">{{ config('app.url') }}</a></h4>
            </div>
        </div>
    </div>
</body>
</html>