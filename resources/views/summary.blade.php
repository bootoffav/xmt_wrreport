@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-4">
            @include('partials.request-sources-chart')
        </div>
        <div class="col-4">
            <div class="row">
                <div class="col-md-12"><h4 class="text-center">Department allocation</h4></div>
                <div class="col-md-12">
                    <canvas id="departmentsAllocation-chart"></canvas>
                    @include('partials.chart', [
                        'requests' => $amountForEveryDepartment,
                        'chartholder' => 'departmentsAllocation',
                        'type' => 'bar',
                        'label' => 'Department allocation'
                    ])
                </div>
                @foreach (array_chunk($amountForEveryDepartment, 7, true) as $chunk)
                <div class="col-md-6">
                    <ul class="list-group">
                        @foreach($chunk as $dep => $depAmount)
                        <li class="list-group-item"><a href="/department/{{ $dep }}">{{ formatDepName($dep) }}: {{ $depAmount }}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-4">
            @include('partials.month-quarter-year-sale-chart')
        </div>
    </div>


    <div class="row mt-2">
        <div class="col-md-4">
            @include('partials.month-quarter-year-chart')
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12"><h4 class="text-center">Requests from countries <a href="/country/all">(see all)</a></h4></div>
                <div class="col-md-12">
                    <canvas id="country-chart"></canvas>
                    @php arsort($countries); @endphp
                    @include('partials.chart', [
                        'requests' => $countries,
                        'chartholder' => 'country',
                        'label' => 'Countries'
                    ])
                </div>
                @for ($k = 0; $k !== 2; $k++)
                <div class="col-md-6">
                    <ul class="list-group">
                    @for ($i = 0; $i < 6; $i++)
                        @php
                            $country = key($countries);
                            $countryAmount = array_shift($countries);
                        @endphp
                        <li class="list-group-item">
                            <a href="/country/{{ $country }}">{{ $country }}: {{ $countryAmount }}</a>
                        </li>
                    @endfor
                    </ul>
                </div>
                @endfor
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <iframe src="requestedProducts" height="1200px;" width="100%"></iframe>
            </div>
        </div>
    </div> 
</div>
@endsection