@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">{{ $subject }}</th>
                    <th class="">Country</th>
                    <th class="">Total Revenue:<br> ${{ number_format($revenueSum, 2, ',', ' ') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($requests as $r)
                    <tr class="request">
                        <td>{{ ++$counter }}</td>
                        <td>
                            <a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/" target="_blank">{{ $r->TITLE}}</a>
                        </td>
                        <td>{{ $r->country }}</td>
                        <td width="15%">${{ number_format((int) $r->revenue, 2, ',', ' ') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-5">{{ $requests->links() }}</div>
    </div>
</div>
@endsection