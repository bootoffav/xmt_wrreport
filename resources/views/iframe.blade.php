<head>
  <base target="_parent" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    @yield('styles')
    @viteReactRefresh
      @vite([
        'resources/assets/sass/app.scss',
        'resources/assets/js/app.tsx'
        ])
    @if (isset($page))
      @inertiaHead
    @endif
</head>
@inertia