@extends('app')

@php
$props = ['requests', 'country', 'manager', 'sale', 'reply', 'product'];
@endphp


@section('content')
<div class="container">
    <h3 class="text-center">{{ $subject }}</h3>
    @if((preg_match('/requests|department|user/i', url()->current()) === 1) and session('datefilter'))
        @include( 'partials.requestsSummary', [ 
            'targetView' => 'web'
        ])
    @endif
    <div class="row">
        <div class="col">
            <table class="table table-bordered table-cover">
                <thead>
                    <tr>
                        <th>#</th>
                        @if (str_contains(url()->current(), 'sale'))
                            <th>Year</th>
                            <th>Company</th>
                        @endif
                        @foreach ($props as $prop)
                            <th>{{ ucfirst($prop) }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                @foreach ($requests as $r)
                    <tr @class([
                        "request",
                        "table-warning" => isset($r->handled) and $r->handled['responseTimeInMin'] > 1440, // if response time more 24h
                    ])>
                        <td>{{ ++$counter }}</td>
                        @if (str_contains(url()->current(), 'sale'))
                            <td>@include('partials.sale', ['param' => 'year'])</td>
                            <td>
                                @if (is_array($r->companies))
                                    @foreach ($r->companies as $company)
                                        <p>{{ $company }}</p>
                                    @endforeach
                                @else
                                    {{ "company not specified" }}
                                @endif
                            </td>
                        @endif
                        <td><a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/" target="_blank">{{ $r->TITLE }}</a></td>
                        @foreach ($props as $prop)
                            @switch($prop)
                                @case('country')
                                    <td><a href="/country/{{ $r->country }}">{{ $r->country }}</a></td>
                                    @break
                                @case('manager')
                                    <td><a href="/user/{{ $r->RESPONSIBLE_ID }}">{{ $realName_id[$r->RESPONSIBLE_ID]['NAME'] }}</a></td>
                                    @break
                                @case('sale')
                                    <td align="right">@include('partials.sale', ['param' => 'amount'])</td>
                                    @break
                                @case('product')
                                    <td>
                                    @unless (is_null($r->products))
                                        @foreach ($r->products as $p)
                                            <p><a href="{{ route('product', ['product' => $p] )}}">{{ $p }}</a></p>
                                        @endforeach
                                    @endunless
                                    </td>
                                    @break
                                @case('reply')
                                    <td>
                                        @isset($r->handled)
                                            @if ($r->handled['type'] === 'commentInTask')
                                                <a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/?MID={{ $r->handled['id'] }}"
                                                target="_blank">{{ $r->handled['responseTimeFormatted'] }}
                                                </a>
                                            @else
                                                {{ $r->handled['responseTimeFormatted'] }} 
                                            @endif
                                        @endisset
                                    </td>
                            @endswitch
                        @endforeach
                    </tr>
                @endforeach
                <tr>
                    <td colspan={{ str_contains(url()->current(), 'sale') ? '7' : '5'}}></td>
                    <td colspan="2">
                        @if (isset($totalHandlingTime) and isset($avgHandlingTime))
                            <div>TOTAL: {{ $totalHandlingTime }}</div>
                            <div>AVG: {{ $avgHandlingTime }}</div>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
       </div>
    </div>
    <div class="row align-items-center">
        <div class="col-4">
            <label for="itemsPerPage">show items per page</label>
            <select class="form-select-sm" id="itemsPerPage" onchange="changeShowItemsPerPage(this)">
                @foreach (['15', '30', '50', '100'] as $value)
                <option value="{{ $value }}" {{ session('showItemsPerPage') == $value ? 'selected' : ''}}>{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="col">
            {{ $requests->links() }}
        </div>
    </div>
</div>
@endsection
