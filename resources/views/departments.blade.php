@extends('app')
@section('header')
    @parent
    <ul class="nav nav-tabs nav-fill" role="tablist">
        @foreach (array_keys($inDep) as $dep)

            <li class="nav-item" role="presentation">
                <a 
                @class([
                    "nav-link",
                    'active' => $loop->first
                ])
                href="#{{ $dep }}" aria-controls="{{ $dep }}" role="tab" data-bs-toggle="tab">{{ formatDepName($dep) }}</a>
            </li>
        @endforeach
    </ul>
@endsection

@section('content')
<div class="tab-content">
    @foreach ($inDep as $dep => $requests)
    <div role="tabpanel" class="tab-pane {{ $loop->first ? 'active' : ''}}" id="{{ $dep }}">
        <div class="row">
            <div class="col">
            @include('partials.departmentTable', [
                'requests' => $requests,
                'total' => array_sum($requests)
            ])
            </div>
            <div class="col d-flex justify-content-center depChartSize">
                <canvas id="{{ $dep }}-chart"></canvas>
                @include('partials.chart', [
                    'chartholder' => $dep,
                    'requests' => $requests,
                    'options' => '
                        legend: {
                            display: true
                        }'
                ])
            </div>
            <div class="col">
                @if (isset($rpc))
                    @includeUnless(empty($rpc[$dep]), 'partials.requestsSummary',
                        [
                            'rpc' => $rpc[$dep],
                            'targetView' => 'web',
                        ])
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
