@php
use App\Helpers\Formatters as fmt;
@endphp
<!DOCTYPE html>
<html lang="en">
@section('head')
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    @yield('styles')
    @viteReactRefresh
    @vite([
      'resources/assets/sass/app.scss',
      'resources/assets/js/app.tsx'
      ])
    @if (isset($page))
      @inertiaHead
    @endif
@show
</head>
<body>
@error('datefilter')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
@section('header')
@auth
<nav class="navbar navbar-expand-lg bg-primary-subtle px-2">
  <h4 class="navbar-brand" id="howManyRequests">
      <a href="{{ route('requests') }}">Web Requests: {{ $howManyRequests }}</a>
  </h4>
  <div class="container">
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav nav-fill w-100">
          <li class="nav-item">
            <a
            @class([
              'nav-link', 'fs-5',
              'active' => url()->current() === config('app.url')
            ])
            aria-current="page" href="/">Summary</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => str_contains(url()->current(), 'department')
              ])
              aria-current="page" href="/department/all">Departments</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => url()->current() === route('companies')
              ])
              aria-current="page" href="{{ route('companies') }}">Companies</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => str_contains(url()->current(), 'country')
              ])
              aria-current="page" href="/country/all">Countries</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => str_contains(url()->current(), 'product')
              ])
              aria-current="page" href="/product/all">Products</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => str_contains(url()->current(), 'sale')
              ])
              aria-current="page" href="/sale/all">Sales</a>
          </li>
          <li class="nav-item">
              <a
              @class([
                'nav-link', 'fs-5',
                'active' => str_contains(url()->current(), 'requestsToEmail')
              ])
              aria-current="page" href="/requestsToEmail">Requests to email</a>
          </li>
        </ul>
    </div>
  </div>
  <div class="d-flex flex-column">
    <div
      class="btn-group"
      role="group">
      <button
        type="button"
        class="btn btn-sm btn-link dropdown-toggle text-end"
        data-bs-toggle="dropdown"
        aria-expanded="false">Options</button>
      <ul class="dropdown-menu dropdown-menu-end">
        <li><a href="{{ route('update') }}" type="button" class="dropdown-item">Update (500)</a></li>
        <li><a href="{{ route('updateAll') }}" type="button" class="dropdown-item">Update (all)</a></li>
        <li><a href="{{ route('department_report') }}" type="button" class="dropdown-item">Department report (.xls)</a></li>
        <li>
          <form action={{ url('/logout') }} method="POST">
          @csrf
          <button class="dropdown-item" type="submit">Logout</button>
          </form>
        </li>
      </ul>
    </div>
    <button
      class="btn btn-sm btn-link"
      data-bs-toggle="modal"
      data-bs-target="#dates"
      id="dateBtn"
    >@if (session()->has('datefilter'))
      {{ fmt::formatDate(
        session('datefilter')
      ) }}
    @else
      Date filter
    @endif
    </button>
  </div>
</nav>
@endauth
@include('partials.dates')
@show

<div class="mt-2">
@yield('content')
</div>
@yield('scripts')
@if (isset($page))
  @inertia
@endif
</body>
</html>
