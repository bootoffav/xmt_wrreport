@extends('app')

@section('content')
<div class="container my-2">
  <div class="row">
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
            <th class="">#</th>
            <th class="">{{ ucfirst($item) }}</th>
            <th class="">Web request task name</th>
            <th class="">Country</th>
            <th class="">Manager</th>
            <th class="">Reply</th>
            <th class="">All Products</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($requests as $itemName => $reqs)
      <tr>
          <td rowspan="{{ count($reqs) }}">{{ ++$counter }}</td>
          <td rowspan="{{ count($reqs) }}">
              @if ($item === 'company')
                {{ $itemName }}
              @else
                <a href="/{{$item}}/{{$itemName}}">{{$itemName}}</a>
              @endif
          </td>
          @foreach ($reqs as $r)
          <td><a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/" target="_blank">{{ $r->TITLE }}</a></td>
          <td><a href="/country/{{ $r->country }}">{{ $r->country }}</a></td>
          <td><a href="/user/{{ $r->RESPONSIBLE_ID }}">{{ $realName_id[$r->RESPONSIBLE_ID]['NAME'] }}</a></td>
          <td>
            @isset($r->handled)
            @if ($r->handled['type'] === 'commentInTask')
                <a href="{{ config('app.b24_hostname') }}/company/personal/user/{{ config('app.b24_user_id') }}/tasks/task/view/{{ $r->ID }}/?MID={{ $r->handled['id'] }}"
                target="_blank">{{ $r->handled['responseTimeFormatted'] }}
                </a>
            @else
                {{ $r->handled['responseTimeFormatted'] }} 
            @endif
        @endisset
          </td>
          <td>
            @unless (is_null($r->products))
              @foreach ($r->products as $p)
              <a href="{{ route('product', ['product' => $p] )}}">{{ $p }}</a>
              @endforeach
            @endunless
          </td>
        </tr>
          @endforeach
      @endforeach
        <tr>
          <td colspan="5"></td>
          <td colspan="2">
              @if (isset($totalHandlingTime) and isset($avgHandlingTime))
                  <div>TOTAL: {{ $totalHandlingTime }}</div>
                  <div>AVG: {{ $avgHandlingTime }}</div>
              @endif
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="row align-items-center">
    <div class="col-4">
      <label for="itemsPerPage">items per page</label>
      <select class="form-select-sm" id="itemsPerPage" onchange="changeShowItemsPerPage(this)">
          @foreach (['15', '30', '50', '100'] as $value)
          <option value="{{ $value }}" {{ session('showItemsPerPage') == $value ? 'selected' : ''}}>{{ $value }}</option>
          @endforeach
      </select>
    </div>
    <div class="col">
        {{ $requests->links() }}
    </div>
  </div>
</div>
@endsection