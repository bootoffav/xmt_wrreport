@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th class="">#</th>
                    <th class="">Web Requests of every Year</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($eachMonth as $year => $months)
                    @foreach ($months as $month => $requestsPerMonth)
                        <tr>
                        <td>
                            <a href="/{{ $subject === 'Sale' ? 'sale' : 'month'}}/{{ $year }}-{{ $month }}">{{ $month }} {{ $year }}</a>
                        </td>
                        <td>
                            {{ count($requestsPerMonth) }}
                        </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection