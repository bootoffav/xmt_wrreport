<?php

namespace App\B24\Traits;

use Carbon\Carbon;

trait APIRequestsWebRequestHandling
{
    public function getResponsibleUserLatestCommentInTask($taskId, $userId)
    {
        $allComments = json_decode(
            $this->client->request('GET', 'task.commentitem.getlist'.'?'.
                http_build_query([
                    'TASKID' => $taskId,
                    'ORDER' => ['ID' => 'asc'],
                    'FILTER' => [
                        'AUTHOR_ID' => $userId,
                    ],
                ])
            )->getBody()
        );

        if (isset($allComments->result) and count($allComments->result)) {
            $allComments->result[0]->CREATED = $allComments->result[0]->POST_DATE;
            unset($allComments->result[0]->POST_DATE);
        }

        return $allComments->result[0] ?? null;
    }

    public function getResponsibleUserLatestCommentInRelatedEntities(
        string $userId,
        array $relatedEntities,
        string $taskCreationDate
    ) {
        $relatedEntities = $this->formRelatedEntitiesFromRaw($relatedEntities);

        $reqToAPI = function ($carry, $curEntity) use ($userId, $taskCreationDate) {
            $request = fn ($start = 0) => json_decode(
                $this->client->request('GET', 'crm.timeline.comment.list'.'?'.
                    http_build_query([
                        'SELECT' => [
                            'ID', 'CREATED',
                            'COMMENT', // presented for testing purposes
                        ],
                        'FILTER' => [
                            'ENTITY_TYPE' => $curEntity['type'],
                            'ENTITY_ID' => $curEntity['id'],
                            'AUTHOR_ID' => $userId,
                        ],
                        'start' => $start,
                    ])
                )->getBody()
            );

            $response = $request();

            if ($response->total >= 50) {
                $response = $request(intdiv($response->total, 50) * 50);
            }

            if (end($response->result)) {
                $commentCreationDate = end($response->result)->CREATED;
                // check if comment later than task created
                if (Carbon::parse($commentCreationDate)->gt($taskCreationDate)) {
                    $carry[] = end($response->result);
                }
            }

            return $carry;
        };

        $commentsInRelatedEntities = array_reduce(
            $relatedEntities,
            $reqToAPI,
            []
        );

        if (count($commentsInRelatedEntities) > 1) {
            usort($commentsInRelatedEntities, function ($a, $b) {
                if ($a->ID < $b->ID) {
                    return 1;
                }

                if ($a->ID > $b->ID) {
                    return -1;
                }

                return 0;
            });
        }

        return $commentsInRelatedEntities[0] ?? null;
    }

    protected function formRelatedEntitiesFromRaw($relatedEntities, $ownerType = false)
    {
        $typeMap = [
            'CO' => 'company', 'L' => 'lead', 'C' => 'contact', 'D' => 'deal',
        ];
        $typeMapForOwnerType = [
            'CO' => '4', 'C' => '3', 'L' => '1', 'D' => '2',
        ];

        return array_map(function ($entity) use ($typeMap, $ownerType, $typeMapForOwnerType) {
            [$type, $id] = explode('_', $entity);

            return [
                'type' => $ownerType ? $typeMapForOwnerType[$type] : $typeMap[$type],
                'id' => $id,
            ];
        }, $relatedEntities);
    }

    public function getActivitiesOfRelatedEntities(
        $relatedEntities,
        string $responsibleId,
        string $created
    ) {
        $relatedEntities = $this->formRelatedEntitiesFromRaw($relatedEntities, true);

        $reqToAPI = function ($acc, $re) use ($responsibleId, $created) {
            $lastActivities = json_decode(
                $this->client->request('GET', 'crm.activity.list'.'?'.
                    http_build_query([
                        'SELECT' => [
                            'ID',
                            'CREATED',
                            'SUBJECT',
                            'DESCRIPTION',
                            'TYPE_ID',
                        ],
                        'FILTER' => [
                            'OWNER_ID' => $re['id'],
                            'OWNER_TYPE_ID' => $re['type'],
                            'AUTHOR_ID' => $responsibleId,
                            '>CREATED' => $created,
                        ],
                        'ORDER' => [
                            'id' => 'asc',
                        ],
                    ])
                )->getBody()
            );

            return array_merge($acc, $lastActivities->result ?? []);

        };

        return array_reduce(
            $relatedEntities,
            $reqToAPI,
            []
        );
    }
}

// TYPE_ID 1 = meeting
// TYPE_ID 2 = call
// TYPE_ID 3 = task
// TYPE_ID 4 = email
