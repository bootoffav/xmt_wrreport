<?php

namespace App\B24\Traits;

trait APIRequests
{
    /**
     * Gets all task created by Web Request user
     *
     * @return array
     */
    public function getAllWebRequests($limit = null)
    {
        $data = [];
        $page = 1;

        do {
            $response = json_decode(
                $this->client->request('GET', 'task.item.list'.'?'.
                    http_build_query([
                        'ORDER' => ['ID' => 'desc'],
                        'FILTER' => [
                            'CREATED_BY' => config('app.b24_user_id'),
                        ],
                        'PARAMS' => [
                            'NAV_PARAMS' => ['nPageSize' => 50, 'iNumPage' => $page++],
                        ],
                        'SELECT' => ['TITLE', 'DESCRIPTION', 'RESPONSIBLE_ID', 'CREATED_DATE', 'TAG', 'UF_CRM_TASK',
                            config('app.b24_country_field'), config('app.b24_request_page'),
                        ],
                    ])
                )->getBody()
            );
            foreach ($response->result as &$req) {
                unset($req->{'ALLOWED_ACTIONS'});
                unset($req->{'GROUP_ID'});
            }
            usleep(1_000_000);
            $data = array_merge($data, $response->result);
            if (isset($limit) and $response->next >= $limit) {
                break;
            }
        } while (isset($response->next));

        return $data;
    }

    public function getUserRealName($userId = null)
    {
        usleep(500_000);
        $query = [
            'start' => 0,
        ];
        if (! is_null($userId)) {
            $query['id'] = $userId;
        }

        $acc = [];
        do {
            $response = json_decode($this->client->request('GET', 'user.get'.'?'.http_build_query($query))->getBody());
            $acc = array_merge($acc, $response->result);
            $query['start'] = $response->next ?? 0;
        } while (isset($response->next));

        $to_ret = [];
        foreach ($acc as $user) {
            $to_ret[$user->ID] = [
                'NAME' => $user->NAME.' '.$user->LAST_NAME,
                'EMAIL' => $user->EMAIL,
            ];
        }

        return $to_ret;
    }

    public function getUserIdByRealName($fullName)
    {
        usleep(500_000);
        [$firstName, $lastName] = explode(' ', $fullName);
        $response = json_decode(
            $this->client->request('GET', 'user.get'.'?'.
                http_build_query([
                    'ORDER' => ['ID' => 'desc'],
                    'FILTER' => ['NAME' => $firstName, 'LAST_NAME' => $lastName],
                ])
            )->getBody()
        );

        return $response->result[0]->ID;
    }

    public function getUsersInDepartment($departmentId): array
    {
        usleep(500000);
        $response = json_decode(
            $this->client->request('GET', 'user.get'.'?'.
                http_build_query([
                    'ORDER' => ['ID' => 'desc'],
                    'FILTER' => ['UF_DEPARTMENT' => $departmentId, 'ACTIVE' => true],
                ])
            )->getBody()
        );

        return array_map(function ($user) {
            return $user->ID;
        }, $response->result);
    }

    public function getAllDepartments()
    {
        usleep(500000);
        $response = json_decode(
            $this->client->request('GET', 'department.get'.'?'.
                http_build_query([
                    'ORDER' => ['ID' => 'desc'],
                ])
            )->getBody()

        );

        return $response->result;
    }

    public function getRevenue($taskId)
    {
        usleep(300000);
        $response = json_decode(
            $this->client->request('GET', 'task.checklistitem.getlist'.'?'.
                http_build_query([
                    'TASKID' => $taskId,
                ])
            )->getBody()
        );

        if (count($response->result) > 0) {
            $revenues = array_map(function ($checklist_item) {
                return $checklist_item->TITLE;
            }, $response->result);
        }

        return $revenues ?? null;
    }

    public function get_company_id_title_map()
    {
        $acc = [];
        do {
            $start = $response->next ?? 0;
            $response = json_decode(
                $this->client->request('GET', 'crm.company.list'.'?'.
                    http_build_query([
                        'start' => $start,
                        'SELECT' => ['ID', 'TITLE', 'ASSIGNED_BY_ID'],
                    ])
                )->getBody()
            );
            $acc = array_merge($acc, $response->result);
            usleep(700000);
        } while (isset($response->next));

        foreach ($acc as $item) {
            $company_id_title_map[$item->ID] = [
                'TITLE' => $item->TITLE,
                'ASSIGNED_BY_ID' => $item->ASSIGNED_BY_ID,
            ];
        }

        return $company_id_title_map;
    }
}
