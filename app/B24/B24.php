<?php

namespace App\B24;

use GuzzleHttp\Client;

class B24
{
    use Traits\APIRequests;
    use Traits\APIRequestsWebRequestHandling;

    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('app.b24_rest_endpoint'),
        ]);
    }
}
