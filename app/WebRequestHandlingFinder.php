<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

// получить запрос

/* процесс поиска события обработки в битриксе:
1. Искать первый комментарий пользователя за которым закреплена задача (иск. пользователей: Vit, Web Request)
2. Искать в связанных компаниях и контактах событие типа email, звонок, комментарий начиная от даты создания задачи


// хранение
allWebRequests requests augmented by the field handled, it is array: [time, commentId]
*/

class WebRequestHandlingFinder
{
    public $reqs;

    public $handlingLimit = 1000;

    public function __construct()
    {
        $this->reqs = array_slice(
            Cache::get('allWebRequests'),
            0,
            $this->handlingLimit
        );
    }

    public function handle()
    {
        foreach ($this->reqs as $ind => $r) {

            $taskId = $r->ID;
            $userId = $r->RESPONSIBLE_ID;
            $taskCreationDate = $r->CREATED_DATE;

            $latestCommentInTask = \B24::getResponsibleUserLatestCommentInTask($taskId, $userId);
            usleep(700_000);
            $latestCommentInRelatedEntities = $r->UF_CRM_TASK ? \B24::getResponsibleUserLatestCommentInRelatedEntities($userId, $r->UF_CRM_TASK, $taskCreationDate) : null;
            usleep(700_000);
            $activities = $r->UF_CRM_TASK ? \B24::getActivitiesOfRelatedEntities(
                $r->UF_CRM_TASK,
                $r->RESPONSIBLE_ID,
                $r->CREATED_DATE
            ) : [];
            usleep(700_000);
            $earliestAction = $this->getEarliestActivity(
                array_filter(
                    array_merge(
                        [$latestCommentInTask],
                        [$latestCommentInRelatedEntities],
                        $activities
                    )
                )
            );
            if ($earliestAction) {
                $responseTimeInMin = (new Carbon($taskCreationDate))->diffInMinutes(new Carbon($earliestAction->CREATED));
                $this->reqs[$ind]->handled['responseTimeInMin'] = $responseTimeInMin;
                $this->reqs[$ind]->handled['responseTimeFormatted'] = self::convertPostedInMinutesToReadableFormat($responseTimeInMin);
                $this->reqs[$ind]->handled['type'] = $this->findTypeOfAction($earliestAction);
                $this->reqs[$ind]->handled['id'] = $earliestAction->ID;
            }

        }

        $restOfReqs = array_slice(
            cache('allWebRequests'),
            $this->handlingLimit
        );
        $reqsToPut = array_merge($this->reqs, $restOfReqs);
        Cache::put(
            'allWebRequests',
            $reqsToPut,
            Caching::$CACHE_TTL
        );

    }

    public function getEarliestActivity($activities)
    {
        if (count($activities)) {
            return array_reduce(
                array_slice($activities, 1),
                // true if current date less than earliest
                fn ($earliest, $cur) => (new Carbon($cur->CREATED))->lt(new Carbon($earliest->CREATED)) ? $cur : $earliest,
                array_slice($activities, 0, 1)[0]
            );
        }
    }

    protected function findTypeOfAction($action): string
    {
        if (property_exists($action, 'POST_MESSAGE')) {
            return 'commentInTask';
        }
        if (property_exists($action, 'TYPE_ID')) {
            switch ($action->TYPE_ID) {
                case '1':
                    return 'meeting';
                    break;
                case '2':
                    return 'call';
                case '3':
                    return 'task';
                case '4':
                    return 'email';
            }
        }

        return 'commentInEntity';
    }

    // 0h:11m
    // 3h:13m
    // 2d:4h:23m
    public static function convertPostedInMinutesToReadableFormat(int $postedInMinutes): string
    {
        if ($postedInMinutes < 60) { // less an hour
            return "0h:${postedInMinutes}m";
        }
        if ($postedInMinutes < 1440) { // less a day
            return intdiv($postedInMinutes, 60).'h:'.$postedInMinutes % 60 .'m';
        }
        $days = intdiv($postedInMinutes, 1440);
        $remainingMinutesAfterDaysSubstructed = $postedInMinutes % 1440;

        if ($remainingMinutesAfterDaysSubstructed > 60) {
            $hours = intdiv($remainingMinutesAfterDaysSubstructed, 60);
            $minutes = $remainingMinutesAfterDaysSubstructed % 60;
        } else {
            $hours = 0;
            $minutes = $remainingMinutesAfterDaysSubstructed;
        }

        return "{$days}d:{$hours}h:{$minutes}m";
    }
}
