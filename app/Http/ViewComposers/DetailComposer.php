<?php

namespace App\Http\ViewComposers;

use App\WebRequest;
use Illuminate\View\View;

class DetailComposer
{
    /**
     * Bind data to the view.
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('howManyRequests', count((new WebRequest)->requests));
    }
}
