<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\FilterRequestListTrait;
use App\Mail\RequestsForPeriod;
use App\Mail\RequestsOfEntity;
use App\RequestPeriodComparison;
use App\WebRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Inertia\Response;

class RequestsToEmailController extends Controller
{
    use FilterRequestListTrait;

    public function __construct(public Request $r) {}

    public function requestsToEmail(WebRequest $wr): Response
    {
        // get managers that have web-requests
        $managers = array_filter(array_map(function ($m) {
            return $m['NAME'];
        }, cache('realName_id')),
            fn ($managerId) => isset($wr->byManager()[$managerId]),
            ARRAY_FILTER_USE_KEY
        );

        // get departments that have web-requests
        $departments = array_filter(
            array_keys(config('app.b24_departments')),
            fn ($depName) => ! empty($wr->inDep()[$depName])
        );
        foreach ($departments as $dept) {
            $deps[$dept] = formatDepName($dept);
        }

        return inertia('requestsToEmail', [
            'managers' => $managers,
            'departments' => $deps,
            'recipients' => config('app.requests_recipients'),
        ]);
    }

    /**
     * @param  $r->input('manager')  Represents user id
     */
    public function requestsOfUserToEmail(
        Request $r,
        WebRequest $wr
    ) {

        $userId = $r->input('manager');
        $typeSelector = $r->input('typeSelector');

        $userRequests = array_map(function ($r) {
            $r->type = RequestPeriodComparison::getRequestType($r);

            return $r;
        },
            $wr->byManager()[$userId]
        );

        $userRequestsNoDatesApplied = (new WebRequest(false))->byManager()[$userId];

        if ($r->session()->has('datefilter')) {
            $this->rpc = new RequestPeriodComparison(
                $userRequests,
                $userRequestsNoDatesApplied
            );
        }

        $reqs = $this->isRequestsShouldBeFiltered() ? $this->getRequestList($userRequests) : $userRequests;

        $rcpts = $this->getRecepients($r);
        Mail::to($rcpts)->send(
            (new RequestsOfEntity(
                $reqs,
                cache('realName_id')[$userId]['NAME'],
                'user'.'/'.$userId,
                ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
            ))->onConnection('sync')
        );
    }

    /**
     * makes array to email recipients
     *
     * @param  $r  Request instance
     */
    protected function getRecepients(Request $r): array
    {
        $rcpts = [];
        if ($r->input('sendToMe')) {
            $rcpts[] = \Auth::user()->email;
        }
        if ($r->input('sendToManager')) {
            $rcpts[] = cache('realName_id')[$r->input('manager')]['EMAIL'];
        }
        if ($r->input('otherEmails')) {
            $otherEmails = explode(' ', $r->input('otherEmails'));
            $rcpts = array_merge($rcpts, $otherEmails);
        }

        return $rcpts;
    }

    public function RequestsOfDepartmentToEmail(
        Request $r,
        WebRequest $wr
    ) {
        $depName = $r->input('department');

        $depRequests = array_map(function ($r) {
            $r->type = RequestPeriodComparison::getRequestType($r);

            return $r;
        },
            array_merge(...array_values($wr->inDep()[$depName]))
        );
        // sort by desc dates
        $depRequests = WebRequest::sort($depRequests);

        $depRequestsNoDatesApplied = array_merge(...array_values(((new WebRequest(false))->inDep()[$depName])));

        if ($r->session()->has('datefilter')) {
            $this->rpc = new RequestPeriodComparison(
                $depRequests,
                $depRequestsNoDatesApplied
            );
        }

        $reqs = $this->isRequestsShouldBeFiltered() ? $this->getRequestList($depRequests) : $depRequests;

        $rcpts = $this->getRecepients($r);
        Mail::to($rcpts)->send(
            (new RequestsOfEntity(
                $reqs,
                formatDepName($r->input('department')),
                'department'.'/'.$r->input('department'),
                ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
            ))->onConnection('sync')
        );
    }

    public function RequestsOfPeriodToEmail(
        Request $r,
    ) {
        Mail::to($r->input('recipients'))->send(
            (new RequestsForPeriod(
                session('datefilter'),
                $r
            ))->onConnection('sync')
        );
    }
}
