<?php

namespace App\Http\Controllers;

use App\Helpers\RequestPaginator;
use App\Http\Controllers\Traits\HelperTrait;
use App\Product;
use App\WebRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductController extends Controller
{
    use HelperTrait;

    public function __invoke(Request $r, Product $products, $product): View
    {
        $requests = array_filter($products->requests, fn ($reqs) => $reqs); // remove empty productcs
        $page = new RequestPaginator(
            $r,
            $product === 'all' ? $requests : $products->requests[$product]
        );
        [$avg, $totalHandlingTime] = $this->handlingTime(WebRequest::getRequests());

        return view(
            'filter_'.($product === 'all' ? 'all' : 'detail'), [
                'subject' => "Web Requests of $product",
                'item' => $product != 'all' ?: 'product',
                'requests' => $page->chunk,
                'counter' => $page->offset,
                'avgHandlingTime' => $avg,
                'totalHandlingTime' => $totalHandlingTime,
            ]);
    }
}
