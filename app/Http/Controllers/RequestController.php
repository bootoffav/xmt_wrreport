<?php

namespace App\Http\Controllers;

use App\Helpers\RequestPaginator;
use App\Helpers\DateFilter;
use App\Http\Controllers\Traits\FilterRequestListTrait;
use App\Http\Controllers\Traits\HelperTrait;
use App\RequestPeriodComparison;
use App\WebRequest;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    use FilterRequestListTrait;
    use HelperTrait;
    use DateFilter;

    public $requests;

    public function __construct(public Request $r) {}

    public function all(WebRequest $wr)
    {
        if ($this->r->has('dates')) {
            return redirect()->route('dates', [
                'route' => 'requests',
            ])->withInput();
        }

        if ($this->r->session()->has('datefilter')) {
            $this->rpc = new RequestPeriodComparison($wr->requests);
        }

        $reqs = $this->isRequestsShouldBeFiltered() ? $this->getRequestList() : $wr->requests;
        $page = new RequestPaginator($this->r, $reqs);
        [$totalHandlingTime, $avg] = $this->handlingTime($reqs);

        return view('filter_detail', [
            'subject' => 'Web requests',
            'isAnnualReport' => $this->isAnnualReport(),
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'totalHandlingTime' => $totalHandlingTime,
            'avgHandlingTime' => $avg,
            ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
        ]);
    }
}
