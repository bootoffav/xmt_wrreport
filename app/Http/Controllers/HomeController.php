<?php

namespace App\Http\Controllers;

use App\Country;
use App\Helpers\MonthFilter;
use App\Helpers\QuarterFilter;
use App\Helpers\RequestPaginator;
use App\Helpers\SaleFilter;
use App\Helpers\SourceFilter;
use App\Helpers\YearFilter;
use App\Helpers\DateFilter;
use App\Http\Controllers\Traits\FilterRequestListTrait;
use App\Http\Controllers\Traits\HelperTrait;
use App\Jobs\UpdateCache;
use App\Product;
use App\RequestPeriodComparison;
use App\WebRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class HomeController extends Controller
{
    use FilterRequestListTrait;
    use HelperTrait;
    use DateFilter;

    /*
    * Illuminate\Http\Request instance
    */
    protected $r;

    /**
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->r = $request;

        if (self::isEmptyCache()) {
            return redirect()->action(
                [HomeController::class, 'update'],
            )->with('noData', true);
        }
    }

    protected static function isEmptyCache(): bool
    {
        return collect(['allWebRequests', 'realName_id', 'company_id_title_map'])->every(fn (string $v) => ! Cache::has($v));
    }

    public function summary(
        WebRequest $wr,
        Country $countries,
        Product $products
    ): View {
        // add logic to make 2 charts (years, months) and pass it to view
        $eachYear = $wr->requestsForYears();
        $eachMonth = $wr->requestsForLastMonths();
        $eachQuarter = $wr->eachQuarter();
        $requestsFromSite = $wr->bySources()['site']->requests;
        $requestsFromEmail = $wr->bySources()['email']->requests;
        $eachMonthSale = $wr->requestsForLastMonths(
            12,
            (new SaleFilter($wr->requests))->requests,
            true
        );
        $eachQuarterSale = $wr->requestsForQuarters(
            12,
            (new SaleFilter($wr->requests))->requests,
            true
        );

        $eachYearSale = $wr->requestsForYears(
            12,
            (new SaleFilter($wr->requests))->requests,
            true
        );
        $eachYearSaleSum = array_map(function ($v) {
            preg_match('/ (.*)/', $v, $matches);

            return $matches[1];
        }, $eachYearSale);

        $eachMonthSaleSum = array_map(function ($v) {
            preg_match('/ (.*)/', $v, $matches);

            return $matches[1];
        }, $eachMonthSale);

        $eachQuarterSaleSum = array_map(function ($v) {
            preg_match('/ (.*)/', $v, $matches);

            return $matches[1];
        }, $eachQuarterSale);

        $websiteRequestsByYears = (new YearFilter($requestsFromSite))->requests;
        $websiteRequestsByMonths = (new MonthFilter($requestsFromSite))->requests;
        $emailRequestsByYears = (new YearFilter($requestsFromEmail))->requests;
        $emailRequestsByMonths = (new MonthFilter($requestsFromEmail))->requests;

        $websiteRequestsByLastMonths = [];
        foreach ($websiteRequestsByMonths as $year => $monthReqs) {
            foreach ($monthReqs as $month => $reqs) {
                $websiteRequestsByLastMonths[$month.' '.$year] = $reqs;
                if (count($websiteRequestsByLastMonths) == 12) {
                    break 2;
                }
            }
        }

        $emailRequestsByLastMonths = [];
        foreach ($emailRequestsByMonths as $year => $monthReqs) {
            foreach ($monthReqs as $month => $reqs) {
                $emailRequestsByLastMonths[$month.' '.$year] = $reqs;
                if (count($emailRequestsByLastMonths) == 12) {
                    break 2;
                }
            }
        }

        return view('summary', [
            'amountForEveryDepartment' => $wr->getRequestAmountForEveryDepartment(),
            'countries' => self::getAmount($countries->requests),
            'eachMonth' => $eachMonth,
            'eachYear' => $eachYear,
            'eachQuarter' => $eachQuarter,
            'eachMonthSale' => $eachMonthSale,
            'eachMonthSaleSum' => $eachMonthSaleSum,
            'eachQuarterSale' => $eachQuarterSale,
            'eachQuarterSaleSum' => $eachQuarterSaleSum,
            'eachYearSale' => $eachYearSale,
            'eachYearSaleSum' => $eachYearSaleSum,
            'websiteRequestsByYears' => $websiteRequestsByYears,
            'websiteRequestsByLastMonths' => $websiteRequestsByLastMonths,
            'emailRequestsByYears' => $emailRequestsByYears,
            'emailRequestsByLastMonths' => $emailRequestsByLastMonths,
        ]);
    }

    public function bySources(WebRequest $wr)
    {
        $requestsFromSite = $wr->bySources()['site']->requests;
        $websiteRequestsByYears = (new YearFilter($requestsFromSite))->requests;
        $websiteRequestsByMonths = (new MonthFilter($requestsFromSite))->requests;

        $collector = [];
        foreach ($websiteRequestsByMonths as $year => $monthReqs) {
            foreach ($monthReqs as $month => $reqs) {
                $collector[$year.' '.$month] = $reqs;
                if (count($collector) == 12) {
                    break 2;
                }
            }
        }
    }

    public function sale()
    {
        $sales = new SaleFilter(
            (new WebRequest(false))->requests
        );
        $revenueSum = $sales->revenueSum();
        $page = new RequestPaginator($this->r, $sales->requests);
        [$totalHandlingTime, $avg] = $this->handlingTime($sales->requests);

        return view('filter_detail', [
            'subject' => 'Web Requests leading to sale: '
                .count($sales->requests)
                .', total revenue: '
                .(new \NumberFormatter('en_US', \NumberFormatter::CURRENCY))->formatCurrency($revenueSum, 'USD'),
            'counter' => $page->offset,
            'revenueSum' => $revenueSum,
            'requests' => $page->chunk,
            'totalHandlingTime' => $totalHandlingTime,
            'avgHandlingTime' => $avg,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    public function requestedProducts(WebRequest $wr)
    {
        try {
            $wrWithProducts = WebRequest::withProducts($wr->requests);
            $eachYear = $wr->requestsForYears(10, $wrWithProducts);
            $eachMonth = $wr->requestsForLastMonths(12, $wrWithProducts);
            $lastQuarters = (new QuarterFilter($wrWithProducts))->diagFormat();
            $error = false;
        } catch (\Throwable $e) {
            $error = true;
        }

        return inertia('requestedProducts', [
            'error' => $error,
            'months' => $eachMonth ?? null,
            'years' => $eachYear ?? null,
            'quarters' => $lastQuarters ?? null,
        ]);
    }

    public function salesOfMonth(WebRequest $wr, $year, $month)
    {
        $requests = (new SaleFilter($wr->requests))->requests;
        $page = new RequestPaginator($this->r, $wr->eachMonth($requests)[$year][$month]);

        return view('filter_detail', [
            'subject' => 'Sale Requests of '."$month $year",
            'item' => "$month $year",
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    public function salesOfYear(WebRequest $wr, $year)
    {
        $requests = (new SaleFilter($wr->requests))->requests;
        $page = new RequestPaginator($this->r, $wr->eachYear($requests)[$year]);

        return view('filter_detail', [
            'subject' => 'Sale Requests of '.$year,
            'item' => $year,
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    public static function getAmount(array $requests): array
    {
        if (array_key_exists('lithuania', $requests)) {
            foreach ($requests as $depName => $usersReqs) {
                foreach ($usersReqs as $user => $reqs) {
                    $ret[$depName][$user] = count($reqs);
                }
            }

            ksort($ret);

            return $ret;
        }
        array_walk($requests, function (&$value) {
            $value = count($value);
        });

        return $requests;
    }

    public function requestsOfUser(WebRequest $wr, $id)
    {
        if ($this->r->has('dates')) {
            return redirect()->route('dates', [
                'route' => 'requestsOfUser',
                'id' => $id,
            ])->withInput();
        }

        $userName = cache(('realName_id'))[$id]['NAME'];
        $userRequests = $wr->byManager()[$id];
        $userRequestsNoDatesApplied = (new WebRequest(false))->byManager()[$id];

        if ($this->r->session()->has('datefilter')) {
            $this->rpc = new RequestPeriodComparison(
                $userRequests,
                $userRequestsNoDatesApplied
            );
        }

        $reqs = $this->isRequestsShouldBeFiltered() ? $this->getRequestList() : $userRequests;
        $page = new RequestPaginator(
            $this->r,
            $reqs
        );

        return view('filter_detail', [
            'subject' => 'Web Requests of '.$userName,
            'item' => $userName,
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'isAnnualReport' => $this->isAnnualReport(),
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
            ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
        ]);
    }

    public function requestsOfYear(WebRequest $wr, $year)
    {
        $page = new RequestPaginator($this->r, $wr->eachYear()[$year]);

        return view('filter_detail', [
            'subject' => 'Web Requests of '.$year,
            'item' => $year,
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    public function requestsOfMonth(WebRequest $wr, $year, $month)
    {
        $page = new RequestPaginator($this->r, $wr->eachMonth()[$year][$month]);

        return view('filter_detail', [
            'subject' => 'Web Requests of '."$month $year",
            'item' => "$month $year",
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    /**
     * @param  Request  $Request
     * @param  WebRequest  $webRequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fromSites(WebRequest $wr, $source)
    {
        $page = new RequestPaginator($this->r, (new SourceFilter($wr->requests, $source))->requests);

        return view('filter_detail', [
            'subject' => 'Web Request from '.ucfirst($source),
            'item' => $source,
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
        ]);
    }

    public function requestsOfAllYears(WebRequest $wr)
    {
        return view('allYears', [
            'eachYear' => $wr->eachYear(),
            'subject' => 'Web',
        ]);
    }

    public function salesOfAllYears(WebRequest $wr)
    {
        $requests = (new SaleFilter($wr->requests))->requests;

        return view('allYears', [
            'eachYear' => $wr->eachYear($requests),
            'subject' => 'Sale',
        ]);
    }

    public function salesOfAllMonths(WebRequest $wr)
    {
        $requests = (new SaleFilter($wr->requests))->requests;

        return view('allMonths', [
            'eachMonth' => $wr->eachMonth($requests),
            'subject' => 'Sale',
        ]);
    }

    public function requestsOfAllMonths(WebRequest $wr)
    {
        return view('allMonths', [
            'eachMonth' => $wr->eachMonth(),
        ]);
    }

    public function update()
    {
        UpdateCache::dispatch($this->r->user()->email, 500);

        return view('update');
    }

    public function updateAll()
    {
        UpdateCache::dispatch($this->r->user()->email);

        return view('update');
    }

    public function approval()
    {
        return view('auth.approval');
    }
}
