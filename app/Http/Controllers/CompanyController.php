<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helpers\RequestPaginator;
use App\Http\Controllers\Traits\HelperTrait;
use App\WebRequest;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    use HelperTrait;

    public function __invoke(Request $r, Company $companies)
    {
        $page = new RequestPaginator($r, $companies->requests);

        foreach (cache(('allWebRequests')) as $req) {
            $wr_id_title_map[$req->ID] = $req->TITLE;
        }
        [$totalHandlingTime, $avg] = $this->handlingTime(WebRequest::getRequests());

        return view('filter_all', [
            'url' => 'company',
            'item' => 'company',
            'requests' => $page->chunk,
            'companyNames' => $companies->company_id_title_map,
            'counter' => $page->offset,
            'totalHandlingTime' => $totalHandlingTime,
            'avgHandlingTime' => $avg,
            'webRequest_id_title_map' => $wr_id_title_map,
        ]);
    }
}
