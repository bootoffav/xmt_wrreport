<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function __invoke(Request $r)
    {
        $r->session()->put('showItemsPerPage', $r['showItemsPerPage']);

        return response('success', 200);
    }
}
