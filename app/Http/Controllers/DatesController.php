<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DatesController extends Controller
{
    public function __invoke(Request $r)
    {
        // sets up datefilter when open link not being authenticated, like from email report
        if ($r->isMethod('get')) {
            $route = $r->input('route');
            $id = $r->input('id');

            $r->session()->put(
                'datefilter',
                $r->old('dates')
            );

            return redirect()->route($route,
                [
                    ...($id ? ['id' => $id] : []),
                    'type' => $r->old('type'),
                    'period' => $r->old('period'),
                ]
            );
        }

        if ($r->isMethod('post')) {

            if ($r->input('datefilter') === '') {
                $r->session()->forget('datefilter');

                return back();
            }

            $r->validate([
                'datefilter' => [
                    'regex:/^\d{4}-\d{2}-\d{2}:\d{4}-\d{2}-\d{2}$/',
                ],
            ]);

            $r->session()->put('datefilter', $r->input('datefilter'));
        }

        return back();
    }
}
