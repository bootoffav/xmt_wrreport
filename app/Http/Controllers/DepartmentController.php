<?php

namespace App\Http\Controllers;

use App\Exports\DepartmentReportExport;
use App\Helpers\Formatters as fmt;
use App\Helpers\RequestPaginator;
use App\Http\Controllers\Traits\FilterRequestListTrait;
use App\RequestPeriodComparison;
use App\WebRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\DateFilter;

class DepartmentController extends Controller
{
    use FilterRequestListTrait, DateFilter;

    public function __construct(public Request $r) {}

    public function all(WebRequest $wr)
    {
        foreach (
            [
                ...['other'],
                ...array_keys(config('app.b24_departments')),
            ] as $dep) {
            $depRequests = array_merge(...array_values($wr->inDep()[$dep]));
            $depRequestsNoDatesApplied = array_merge(...array_values((new WebRequest(false))->inDep()[$dep]));
            if ($this->r->session()->has('datefilter')) {
                $this->rpc[$dep] = new RequestPeriodComparison(
                    $depRequests,
                    $depRequestsNoDatesApplied
                );
            }
        }

        return view('departments', [
            'inDep' => HomeController::getAmount($wr->inDep()),
            'isAnnualReport' => $this->isAnnualReport(),
            ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
        ]);
    }

    public function fromDep(WebRequest $wr, $depName)
    {
        $depRequests = array_merge(...array_values($wr->inDep()[$depName]));
        $depRequestsNoDatesApplied = array_merge(...array_values(((new WebRequest(false))->inDep()[$depName])));

        if ($this->r->session()->has('datefilter')) {
            $this->rpc = new RequestPeriodComparison(
                $depRequests,
                $depRequestsNoDatesApplied
            );
        }

        $reqs = $this->isRequestsShouldBeFiltered() ? $this->getRequestList() : $depRequests;
        // sort by desc dates
        $reqs = WebRequest::sort($reqs);

        $page = new RequestPaginator(
            $this->r,
            $reqs
        );

        return view('filter_detail', [
            'subject' => 'Web Requests of '.formatDepName($depName),
            'item' => ucfirst($depName),
            'isAnnualReport' => $this->isAnnualReport(),
            'requests' => $page->chunk,
            'counter' => $page->offset,
            'currentShowItemsPerPage' => (int) $this->r->session()->get('showItemsPerPage'),
            ...(isset($this->rpc) ? ['rpc' => $this->rpc] : []),
        ]);
    }

    private function getDate(): string
    {
        return (session()->has('datefilter')) ? fmt::formatDate(session('datefilter')).' ' : '';
    }

    public function export(WebRequest $wr)
    {
        return Excel::download(new DepartmentReportExport($wr), "XM - {$this->getDate()}Web-requests by Departments.xlsx");
    }
}
