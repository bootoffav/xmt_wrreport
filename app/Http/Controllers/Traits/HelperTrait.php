<?php

namespace App\Http\Controllers\Traits;

use App\WebRequestHandlingFinder;

trait HelperTrait
{
    public function handlingTime($reqs): array
    {
        $totalHandlingTime = array_reduce($reqs, fn ($acc, $r) => isset($r->handled) ? $acc += $r->handled['responseTimeInMin'] : $acc, 0);

        return [
            WebRequestHandlingFinder::convertPostedInMinutesToReadableFormat($totalHandlingTime),
            WebRequestHandlingFinder::convertPostedInMinutesToReadableFormat($totalHandlingTime / count($reqs)),
        ];
    }
}
