<?php

namespace App\Http\Controllers\Traits;

trait FilterRequestListTrait
{
    private function isRequestsShouldBeFiltered(): bool
    {
        return $this->r->has(['type', 'period'])
        and
        $this->r->session()->has('datefilter');
    }

    private function paramsAreCorrect(string $period, string $type): bool
    {
        $correctPeriodParams = ['c', 'p'];
        $correctTypeParams = ['wr', 'email', 'p', 'np', 'all', 'withProducts'];

        return in_array($period, $correctPeriodParams) and in_array($type, $correctTypeParams);
    }

    public function __call($fnName, $args)
    {
        if ($fnName === 'getRequestList') {
            switch (count($args)) {
                case 0:
                    [$period, $type] = [
                        $this->r->query('period'),
                        $this->r->query('type'),
                    ];
                    if ($this->paramsAreCorrect($period, $type)) {
                        $reqs = $period === 'c' ? $this->rpc->curSummary : $this->rpc->prevSummary;

                        return $type === 'all' ? array_merge($reqs['wr'], $reqs['email']) : $reqs[$type];
                    }

                    return array_merge($this->rpc->curSummary['wr'], $this->rpc->curSummary['email']);
                case 1:
                    [$requests] = $args;

                    return array_reduce(
                        $this->r->input('type'),
                        function ($carry, $type) use ($requests) {
                            switch ($type) {
                                case 'email':
                                case 'wr':
                                    $cur = array_filter(
                                        $requests,
                                        fn ($r) => in_array($r->type, $this->r->input('type'))
                                    );
                                    break;
                                case 'p':
                                    $cur = array_filter($requests, fn ($r) => ! empty($r->companies));
                                    break;
                                case 'np':
                                    $cur = array_filter($requests, fn ($r) => empty($r->companies));
                                    break;
                            }

                            $uniqueCur = collect($cur)->filter(function ($v) use ($carry) {
                                return count(array_filter($carry, fn ($r) => $r->ID === $v->ID)) === 0;
                            });

                            return [
                                ...$carry,
                                ...$uniqueCur->all(),
                            ];
                        },
                        []
                    );
            }
        }
    }
}
