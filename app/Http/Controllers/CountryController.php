<?php

namespace App\Http\Controllers;

use App\Country;
use App\Helpers\RequestPaginator;
use App\Http\Controllers\Traits\HelperTrait;
use App\WebRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CountryController extends Controller
{
    use HelperTrait;

    public function __invoke(Request $r, Country $countries, $country): View
    {
        $page = new RequestPaginator(
            $r,
            $country === 'all' ? $countries->requests : $countries->requests[$country]
        );
        [$totalHandlingTime, $avg] = $this->handlingTime(WebRequest::getRequests());

        return view(
            'filter_'.($country === 'all' ? 'all' : 'detail'), [
                'subject' => "Web Requests of $country",
                'url' => 'country',
                'item' => $country != 'all' ?: 'country',
                'totalHandlingTime' => $totalHandlingTime,
                'avgHandlingTime' => $avg,
                'requests' => $page->chunk,
                'counter' => $page->offset,
            ]);
    }
}
