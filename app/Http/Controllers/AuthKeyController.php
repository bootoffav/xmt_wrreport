<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthKeyController extends Controller
{
    public function saveKey(Request $request)
    {
        if (isset($request->code)) {
            $code = $request->code;
            $scope = 'crm,task';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, config('app.b24_hostname')."/oauth/token/?client_id=local.5a97aeaf8ac4c6.29270356&grant_type=authorization_code&client_secret=2hm5z1PJra9OK1jNhvyLHa80MD8wprPlAmQHljBc5xBTdsCfLs&redirect_uri=https%3A%2F%2Fwrreport.xmtextiles.com%2Fget_authkey&code=$code&scope=$scope");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = json_decode(curl_exec($ch));
            $file = fopen(base_path().'/authkey', 'w', true);
            fwrite($file, $response->access_token);
            curl_close($ch);
        }
    }
}
