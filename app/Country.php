<?php

namespace App;

class Country
{
    public $requests;

    public function __construct()
    {
        $this->requests = [];
        foreach (WebRequest::getRequests() as $r) {
            $this->requests[$r->country][] = $r;
        }
        if (array_key_exists('', $this->requests)) {
            $this->requests['No country'] = $this->requests[''];
            unset($this->requests['']);
        }
        ksort($this->requests);
    }
}
