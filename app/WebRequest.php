<?php

namespace App;

use App\Helpers\DateFilter;
use App\Helpers\InDepFilter;
use App\Helpers\ManagerFilter;
use App\Helpers\MonthFilter;
use App\Helpers\SourceFilter;
use App\Helpers\YearFilter;
use App\Helpers\QuarterFilter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class WebRequest
{
    use DateFilter;

    public $requests;

    public $bySales;

    public $eachMonth;

    protected $usersInDep = [];

    public function __construct($applyDates = true)
    {
        $this->requests = self::getRequests($applyDates);
    }

    public static function getRequests($applyDates = true)
    {
        return $applyDates
            ? DateFilter::apply(Cache::get('allWebRequests', []))
            : Cache::get('allWebRequests', []);
    }

    public function eachMonth($requests = null) // only passing for sale
    {
        return (new MonthFilter($requests ?? $this->requests))->requests;
    }

    public function eachYear($requests = null) //only passing for sale
    {
        return (new YearFilter($requests ?? $this->requests))->requests;
    }

    public function eachQuarter()
    {
        return (new QuarterFilter($requests ?? $this->requests, 12))->diagFormat();
    }

    public function inDep()
    {
        return (new InDepFilter($this->usersInDep, $this->byManager()))->requests;
    }

    public function byManager()
    {
        return (new ManagerFilter($this->requests))->requests;
    }

    public function bySources()
    {
        $sources = [];
        foreach (config('app.source') as $source => $label) {
            $sources[$source] = (new SourceFilter($this->requests, $source));
        }

        return $sources;
    }

    public function requestsForLastMonths(
        int $months = 12,
        $requests = null,
        $sale = false
    ) {
        $lastRequests = [];
        $i = 0;
        foreach ($this->eachMonth($requests) as $year => $yearRequests) {
            foreach ($yearRequests as $monthName => $monthRequests) {
                $lastRequests["$monthName $year"] = count($monthRequests);
                // add sum in case of sale
                if ($sale) {
                    $revenueSum = array_reduce($monthRequests, function ($sum, $r) {
                        if ($r->revenue) {
                            array_map(function ($rev) use (&$sum) {
                                $sum += (float) $rev['amount'];
                            },
                                $r->revenue
                            );
                        }

                        return $sum;
                    }, 0);
                    $lastRequests["$monthName $year"] .= " $revenueSum";
                }

                if (++$i === $months) {
                    return $lastRequests;
                }
            }
        }

        return $lastRequests;
    }

    public function requestsForQuarters(
        int $quarters = 12,
        $requests = null,
        $sale = false
    ) {
        $lastRequests = [];
        $i = 0;


        $eachQuarter = array_reduce(
            (new QuarterFilter($requests ?? $this->requests, 12))->requests,
            function ($r, $v) {
                $r[$v['period']] = $v['reqs'];
                return $r;
            },
        );

        foreach ($eachQuarter as $quarter => $quarterRequests) {
            $lastRequests[$quarter] = count($quarterRequests);

            // add sum in case of sale
            if ($sale) {
                $revenueSum = array_reduce($quarterRequests, function ($sum, $r) {
                    if ($r->revenue) {
                        array_map(function ($rev) use (&$sum) {
                            $sum += (float) $rev['amount'];
                        },
                            $r->revenue
                        );
                    }

                    return $sum;
                }, 0);
                $lastRequests[$quarter] .= " $revenueSum";
            }

            if (++$i === $quarters) {
                return $lastRequests;
            }
        }

        return $lastRequests;
    }

    public function requestsForYears(
        int $years = 12,
        $requests = null,
        $sale = false
    ) {
        $lastRequests = [];
        $i = 0;

        foreach ($this->eachYear($requests) as $year => $yearRequests) {
            $lastRequests[$year] = count($yearRequests);

            // add sum in case of sale
            if ($sale) {
                $revenueSum = array_reduce($yearRequests, function ($sum, $r) {
                    if ($r->revenue) {
                        array_map(function ($rev) use (&$sum) {
                            $sum += (float) $rev['amount'];
                        },
                            $r->revenue
                        );
                    }

                    return $sum;
                }, 0);
                $lastRequests[$year] .= " $revenueSum";
            }

            if (++$i === $years) {
                return $lastRequests;
            }
        }

        return $lastRequests;
    }

    public function getRequestAmountForEveryDepartment(): array
    {
        $amountForEveryDep = [];
        array_map(
            function ($dep, $depReqs) use (&$amountForEveryDep) {
                array_map(
                    function ($manReqs) use ($dep, &$amountForEveryDep) {
                        $amountForEveryDep[$dep] = $amountForEveryDep[$dep] ?? 0;
                        $amountForEveryDep[$dep] += count($manReqs);
                    },
                    $depReqs
                );
            },
            array_keys($this->inDep()),
            $this->inDep()
        );

        ksort($amountForEveryDep);

        return $amountForEveryDep;
    }

    /**
     * sort requests by param specified
     *
     * @param  $requests  array of requests
     * @param  $byWhat  string values: 'date'
     * @param  $sortDirection  values: 'desc' | 'asc'
     */
    public static function sort($requests, string $byWhat = 'date', $sortDirection = 'desc'): array
    {
        usort($requests, function ($a, $b) {
            $aDate = Carbon::parse($a->CREATED_DATE);
            $bDate = Carbon::parse($b->CREATED_DATE);

            if ($aDate->lessThan($bDate)) {
                return -1;
            } elseif ($aDate->greaterThan($bDate)) {
                return 1;
            }

            return 0;
        });

        return $sortDirection === 'desc' ? array_reverse($requests) : $requests;
    }

    public static function withProducts($reqs)
    {
        return array_filter(
            $reqs,
            fn ($r) => $r->products
        );
    }
}
