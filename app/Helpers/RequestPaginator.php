<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class RequestPaginator
{
    public $offset;

    /** @var holds current chunk of data produced by pagination */
    public $chunk;

    public function __construct(Request $request, $data_to_paginate)
    {
        $perPage = (int) (session('showItemsPerPage') ?? 15);
        $page = $request->input('page') ?: 1;
        $this->offset = $page * $perPage - $perPage;

        $this->chunk = new LengthAwarePaginator(
            array_slice($data_to_paginate, $this->offset, $perPage, true),
            count($data_to_paginate),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );
    }
}
