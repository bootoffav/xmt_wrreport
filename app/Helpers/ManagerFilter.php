<?php
/**
 * Created by PhpStorm.
 * User: bootoffav
 * Date: 5/16/17
 * Time: 12:19
 */

namespace App\Helpers;

class ManagerFilter
{
    public $requests = [];

    public function __construct($requests)
    {
        foreach ($requests as $request) {
            $this->requests[$request->RESPONSIBLE_ID][] = $request;
        }
    }
}
