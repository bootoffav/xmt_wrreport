<?php

namespace App\Helpers;

use App\Facades\B24;
use Illuminate\Support\Facades\Cache;

class InDepFilter
{
    public $requests = [];

    public function __construct(&$usersInDep, $byManager)
    {
        foreach (config('app.b24_departments') as $depName => $depId) {
            if (! Cache::has($depName)) {
                $users = (is_array($depId)) ? // enclosed departments in B24
                    array_reduce(
                        $depId,
                        fn ($users, $dep) => [...$users, ...B24::getUsersInDepartment($dep)],
                        []
                    )
                    : B24::getUsersInDepartment($depId);
                Cache::put($depName, $users, now()->addDays(5));
            }
            $this->requests[$depName] = []; // init to empty array
            foreach (cache($depName) as $userId) {
                if (array_key_exists($userId, $byManager)) {
                    $this->requests[$depName][$userId] = $byManager[$userId];
                    $usersInDep[] = $userId;
                }
            }
        }
        //other
        $this->requests['other'] = [];
        array_map(
            fn ($id, $reqs) => in_array($id, $usersInDep) ?: $this->requests['other'][$id] = $reqs,
            array_keys($byManager),
            $byManager
        );
        Cache::put('other', array_keys($this->requests['other']));
    }
}
