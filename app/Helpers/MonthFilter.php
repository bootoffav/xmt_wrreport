<?php

namespace App\Helpers;

class MonthFilter
{
    public $requests = [];

    public function __construct($requests)
    {
        foreach ($requests as $r) {
            $year = $this->getYearOfRequest($r);
            $monthNumber = $this->getMonthOfRequest($r);
            $this->requests[$year][$monthNumber][] = $r;
        }
    }

    protected function getYearOfRequest($r)
    {
        return getdate(strtotime($r->CREATED_DATE))['year'];
    }

    protected function getMonthOfRequest($r)
    {
        return getdate(strtotime($r->CREATED_DATE))['month'];
    }
}
