<?php

namespace App\Helpers;

use Carbon\Carbon;

class Formatters
{
    public static $dateFormat = 'Y-m-d';

    public static $dateDelimiter = ':';

    /**
     * @param  $period  string|Carbon representation of date, separated by : like 2023-11-01:2023-11-30
     * @return string like November 2023
     */
    public static function formatDate(string|Carbon|null $period = null): ?string
    {
        if ($period instanceof Carbon) {
            return $period->format('d M Y');
        }

        if (
            is_null($period) or
            ! str_contains($period, self::$dateDelimiter)
        ) {
            return null;
        }

        [$start, $end] = explode(self::$dateDelimiter, $period);
        $start = Carbon::createFromFormat(self::$dateFormat, $start);
        $end = Carbon::createFromFormat(self::$dateFormat, $end);

        if (
            $start->day === 1 and
            $end->isLastOfMonth() and
            $start->isSameMonth($end)
        ) {
            return "{$start->englishMonth} {$start->year}";
        }

        if (
            $start->day === 1 and
            $end->isLastOfMonth() and
            ! $start->isSameMonth($end)
        ) {
            return "{$start->englishMonth} {$start->year} - {$end->englishMonth} {$end->year}";
        }

        return "{$start->format('d M Y')} - {$end->format('d M Y')}";
    }
}
