<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\RequestPeriodComparison;

class QuarterFilter
{
    public $requests = [];

    protected $lastQuarters = []; // 10

    public function __construct(
        $requests = [],
        int $amountOfQuarters = 10,
    ) {
        $this->lastQuarters = $this->getLastQuarters($amountOfQuarters);
        $this->requests = $this->getRequestsForLastQuarters($requests);
    }

    protected function getLastQuarters($amountOfQuarters)
    {
        if (!is_null(session('datefilter'))) {
            [$from, $to] = RequestPeriodComparison::getFromAndTo(session('datefilter'));
            $quarters = [];

            $timePoint = $to->copy();
            do {
                $quarters[] = [
                    'end' => $timePoint->copy()->endOfQuarter(),
                    'start' => $timePoint->copy()->startOfQuarter()
                ];
                $timePoint->startOfQuarter()->subDay();
            } while (
                $timePoint->copy()->endOfQuarter()->gt($from)
                or
                count($quarters) > 9
            );

            return $quarters;
        }
        for ($i = 0; $i < $amountOfQuarters; $i++) {
            $now = Carbon::now()->subQuarter($i);
            $quarter = [
                'start' => $now->copy()->startOfQuarter(),
                'end' => $now->copy()->endOfQuarter(),
            ];
            $quarters[] = $quarter;
        }

        return $quarters;
    }

    protected function getRequestsForLastQuarters($reqs)
    {
        $indexOfRequests = 0;
        $reqs = array_values($reqs);

        return array_map(function ($q) use (&$reqs, &$indexOfRequests) {
            while (
                $indexOfRequests < count($reqs)
                and
                Carbon::create(
                    $reqs[$indexOfRequests]->CREATED_DATE
                )->between(
                    $q['start'],
                    $q['end'],
                    false
                )
            ) {
                $qrWithReqs['reqs'][] = $reqs[$indexOfRequests++];
            }

            $qrWithReqs['period'] = 'Q'.$q['start']->quarter.' '.$q['start']->year;

            return $qrWithReqs;
        }, $this->lastQuarters);
    }

    public function diagFormat()
    {
        return array_reduce(
            $this->requests,
            function ($cur, $q) {
                $cur[$q['period']] = count($q['reqs'] ?? []);

                return $cur;
            },
            []
        );
    }
}
