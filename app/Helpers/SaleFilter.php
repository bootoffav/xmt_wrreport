<?php

namespace App\Helpers;

use App\RequestPeriodComparison;

class SaleFilter
{
    public $requests = [];

    public function __construct($requests, $dates = null) // calling site: $dates = [$pf, $pt]
    {
        $saleRequests = array_filter(
            $requests,
            fn ($r) => static::is_sale_request($r)
        );

        if (! is_null($dates)) {
            $fy = $dates[0]->year;
            $ty = $dates[1]->year;
            // get requests for date filtering period
            $saleRequests = $this->applyFilteringByDates($saleRequests, $fy, $ty);
            // remove sales from requests which are not of the year
            $saleRequests = $this->removeSalesForOtherPeriod($saleRequests, $fy, $ty);
        } elseif (session()->has('datefilter')) {
            [$fd, $td] = RequestPeriodComparison::getFromAndTo(session('datefilter'));
            $fy = $fd->year;
            $ty = $td->year;
            // get requests for date filtering period
            $saleRequests = $this->applyFilteringByDates($saleRequests, $fy, $ty);
            // remove sales from requests which are not of the year
            $saleRequests = $this->removeSalesForOtherPeriod($saleRequests, $fy, $ty);
        }

        // filter case-insensitively in ASC mode
        usort($saleRequests, function ($r1, $r2) {
            try {
                return strcasecmp($r1->companies[0], $r2->companies[0]);
            } catch (\Throwable $th) {
                return 0;
            }
        });
        $this->requests = $saleRequests;
    }

    protected function removeSalesForOtherPeriod(array $saleRequests, int $fy, int $ty): array
    {
        return array_map(function ($r) use ($fy, $ty) {
            $r_copy = clone $r;
            $r_copy->revenue = array_filter($r_copy->revenue, fn ($rev) => (int) $rev['year'] >= $fy and (int) $rev['year'] <= $ty);

            return $r_copy;
        }, $saleRequests);
    }

    protected function applyFilteringByDates(
        array $saleRequests,
        int $fy,
        int $ty
    ): array {
        return array_filter($saleRequests, function ($r) use ($fy, $ty) {
            if (isset($r->revenue)) {
                foreach ($r->revenue as $rev) {
                    $r_year = (int) $rev['year'];
                    if ($r_year >= $fy and $r_year <= $ty) {
                        return true;
                    }
                }
            }
        });
    }

    public function revenueSum()
    {
        return array_reduce($this->requests, function ($sum, $r) {
            return (is_null($r->revenue))
                ? $sum
                : array_reduce($r->revenue, fn ($sum, $rev) => $sum + (float) $rev['amount'], $sum);
        }, 0);
    }

    public static function is_sale_request($r): bool
    {
        return in_array('C_15308', $r->UF_CRM_TASK);
    }
}
