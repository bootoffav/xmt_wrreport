<?php

namespace App\Helpers;

use App\RequestPeriodComparison;
use Carbon\Carbon;

trait DateFilter
{
    public static function apply(
        array $requests,
        ?Carbon $fd = null,
        ?Carbon $td = null
    ): array {
        if (session()->has('datefilter')) {

            if (is_null($fd) or is_null($td)) {
                [$fd, $td] = RequestPeriodComparison::getFromAndTo(session('datefilter'));
            }

            return array_filter($requests, function ($r) use ($fd, $td) {
                $r_date = Carbon::createFromFormat('Y-m-d', substr($r->CREATED_DATE, 0, 10));

                return $fd <= $r_date and $td >= $r_date;
            });
        }

        return $requests;
    }

    public function isAnnualReport()
    {
        if (session()->has('datefilter')) {
            $datefilter = session()->get('datefilter');
            // check if dates are start and end of their years
            if (
                substr($datefilter, 5, 5) === '01-01'
                and
                substr($datefilter, 16) === '12-31'
            ) {
                return true;
            }
        }

        return false;
    }
}
