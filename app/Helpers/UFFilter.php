<?php

namespace App\Helpers;

class UFFilter
{
    public $requests;

    public function __construct($requests)
    {
        $this->requests = array_fill_keys(
            array_keys($this->filter), []
        );
        array_reduce($requests, function ($_, $r) {
            $reqAddedToTheseEntity = [];
            array_reduce($r->UF_CRM_TASK, function ($_, $mark) use ($r, &$reqAddedToTheseEntity) {
                $key = array_search($mark, $this->filter);
                if ($key) {
                    $this->requests[$key][] = $r;
                    $reqAddedToTheseEntity[] = $key;
                }
            });
            if (! is_null($r->products)) {
                array_reduce($r->products, function ($_, $p) use ($r, $reqAddedToTheseEntity) {
                    if (! in_array($p, $reqAddedToTheseEntity)) {
                        isset($this->requests[$p]) ? $this->requests[$p][] = $r : [$r];
                    }
                });

                return;
            }

            array_push($this->requests[''], $r);
        });
    }
}
