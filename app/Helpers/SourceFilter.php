<?php
/**
 * Created by PhpStorm.
 * User: bootoffav
 * Date: 8/4/17
 * Time: 14:08
 */

namespace App\Helpers;

class SourceFilter
{
    public $requests = [];

    public function __construct($requests, string $source)
    {
        $this->requests = array_filter($requests, function ($request) use ($source) {
            $fromForm = $this->fromForm($request->DESCRIPTION);

            return $source === 'site' ? $fromForm : ! $fromForm;
        });
    }

    private function fromForm(string $description, array $searching_patterns = [
        'Request:',
        'Country:',
        'Telephone:',
    ]): bool
    {
        foreach ($searching_patterns as $pattern) {
            if (strpos($description, $pattern) === false) {
                return false;
            }

            return true;
        }
    }
}
