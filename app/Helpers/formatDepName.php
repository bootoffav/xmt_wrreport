<?php

function formatDepName(string $dep): string
{
    $allTimeCapsDeps = [
        'hu_sk_cz' => 'HU,SK,CZ',
        'usa' => 'USA',
    ];

    return $allTimeCapsDeps[$dep] ?? ucfirst($dep);
}
