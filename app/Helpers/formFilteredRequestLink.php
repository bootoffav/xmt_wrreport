<?php

use Illuminate\Http\Request;

/**
 * Generates a link to sort request list by specified params, used in RequestSummary blade template.
 *
 * @param  array  $params  Represents two values: period, type
 * @param  string  $targetView  values: 'web', 'email'
 * @param  string|null  $dep  values: 'lithuania', 'USA' etc, from config('app.b24_departments')
 * @param  string|null  $path  part of uri
 */
function formFilteredRequestLink(
    array $params,
    string $targetView,
    ?string $dep,
    ?string $path
): string {
    [$period, $type] = $params;
    $url = match (true) {
        (bool) $dep => str_replace('/all', '/'.$dep, url()->current()), // when path is department/all
        (bool) $path => config('app.url')."/$path", // when path is provided on controller class
        $targetView === 'email' => config('app.url').'/requests', // when it's rendered to email for all
        default => url()->current()
    };

    return Request::create($url)->fullUrlWithQuery(
        [
            'period' => $period,
            'type' => $type,
            ...(
                ($targetView === 'email' and session('datefilter'))
                ? ['dates' => session('datefilter')]
                : []
            ),
        ]
    );
}
