<?php

namespace App\Helpers;

class YearFilter
{
    public $requests = [];

    public function __construct($requests)
    {
        foreach ($requests as $r) {
            $year = $this->getYearOfRequest($r);
            $this->requests[$year][] = $r;
        }
    }

    protected function getYearOfRequest($r)
    {
        return getdate(strtotime($r->CREATED_DATE))['year'];
    }
}
