<?php

namespace App\Exports;

use App\Helpers\SaleFilter;
use App\WebRequest;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DepartmentReportExport implements FromArray, WithColumnWidths, WithStrictNullComparison, WithStyles
{
    use ExportStyles;

    public $deps = [];

    public function __construct(WebRequest $wr)
    {
        $sales = (new SaleFilter($wr->requests))->requests;
        $totalOfEmployees = 0;
        $totalOfClients = 0;
        $totalOfReqs = 0;
        $totalOfSales = 0;
        $totalSumOfSales = 0;
        $deps = [
            'italy' => ['8560'],
            'lithuania' => ['89', '8640'],
            'poland' => ['8978'],
            'romania' => ['8496', '8554', '8980'],
            'spain' => ['8470'],
            'hu_sk_cz' => ['8596'],
            'turkey' => ['8638'],
            'bangladesh' => ['9008'],
            'brazil' => ['113'],
            'usa' => ['9014'],
            'russia' => ['1847', '87', '8622'],
            'china' => ['8530', '8532'],
            'kazakhstan' => ['8618'],
            'other' => '',
        ];
        foreach ($deps as $dep => $depId) {
            foreach (cache($dep) as $userId) {
                if (! $this->userIsEligibleForReport($userId)) {
                    continue;
                }
                $amountOfClientsForUser = $this->countAmountOfCompaniesAssignedToUser($userId);
                $this->deps[$dep][$userId]['amountOfClients'] = $amountOfClientsForUser;
                $amountOfReqs = $this->countAmountOfReqsAssignedToUser($userId, $wr->requests);
                $this->deps[$dep][$userId]['amountOfReqs'] = $amountOfReqs;
                [$amountOfSales, $sumOfSales] = $this->getSalesOfUser($sales, $userId);
                $this->deps[$dep][$userId]['amountOfSales'] = $amountOfSales;
                $this->deps[$dep][$userId]['sumOfSales'] = $sumOfSales;
                $totalOfSales += $amountOfSales;
                $totalSumOfSales += $sumOfSales;
                $totalOfReqs += $amountOfReqs;
                $totalOfEmployees += 1;
                $totalOfClients += $amountOfClientsForUser;
            }
        }

        array_push(
            $this->footer,
            $totalOfEmployees,
            $totalOfClients,
            $totalOfReqs,
            $totalOfSales,
            $totalSumOfSales,
            $totalOfReqs,
            $totalSumOfSales,
        );
    }

    private function userIsEligibleForReport($userId): bool
    {
        $eligibleUsers = [
            '996', // Erika Zaiarnaja
            '3928', // Paula Slanina
            '5214', // Greta Parmeggiani
            '5216', // Laura Menapace
            '5288', // Achraf EL Kamili
            '1', // Maksim Ivanov
            '5473', // Marijus Klimaitis
            '71', // Jurgita Kinderiene
            '173', // Veaceslav Cojocari
            '238', // Lilija Sinkevic
            '4986', // Patricija Voinic
            '165', // Anzelika Syrevic
            '5238', // Piotr Wysocki
            '5372', // Kamil Bodek
            '5427', // Dominika Wójcik
            '5439', // Karol Januškievič
            '5475', // Marcin Szrajda
            '310', // Victor Copaci
            '3630', // Lucian Ciovica
            '3734', // Mihai Niculae
            '3736', // Fabian Dinu
            '5250', // Adi Ilie
            '2802', // Radu Popescu
            '4064', // Dragos Marinescu
            '5338', // Lilla Sebo
            '181', // Beatriz Regueiro
            '4416', // Iago Aymerich
            '5126', // Loreta Staponkus
            '5306', // Zakhar Petryshyn
            '161', // Marek Křupala
            '3901', // Robert Petykó
            '5240', // Mert Derya
            '5358', // Ozge Ozfidaner
            '5523', // Rengim DEMİRTAŞ
            '1606', // Sanjay Thakur
            '5326', // Juveriya Maner
            '5499', // Wafi Reza
            '23', // Natallia Demidova
            '25', // Alina Clewlow (Dubs)
            '97', // Marina Petrova
            '9', // Peter Grigoriev
            '3898', // Aidar Auezov
            '3900', // Raushan Nurumbetova
            '4930', // Asem Sadybekova
            '5244', // Bakhyt Irgaliyeva
            '5368', // Ekaterina Kovaleva
            '3524', // Ira Danilova
            '5581', // Maja Liška
            '5557', // Marita Tomic
        ];

        return in_array($userId, $eligibleUsers);
    }

    private function getSalesOfUser(array $sales, string $userId): array // amount, sum
    {
        $salesOfUser = array_filter($sales, fn ($s) => $s->RESPONSIBLE_ID === $userId);
        $sumOfSales = array_reduce(
            $salesOfUser,
            function ($sum, $s) {
                $sum += $s->revenue ? array_reduce($s->revenue, fn ($c, $r) => $c += (int) $r['amount'] ?: 0, 0) : 0;

                return $sum;
            },
            0
        );

        return [count($salesOfUser), $sumOfSales];
    }

    private function countAmountOfReqsAssignedToUser(string $userId, array $reqs): int
    {
        return count(array_filter($reqs, fn ($r) => $r->RESPONSIBLE_ID === $userId));
    }

    private function shortenUserName(string $username): string
    {
        $username = explode(' ', $username);

        return substr($username[0], 0, 1).'. '.array_pop($username); // Aleksei Butov -> A. Butov
    }

    public $header = ['Dept.', 'Manager', 'Clients', 'WR, solo', 'Sales (qty)', 'Sales (solo)', 'WR, dept', 'Sales (dept.)'];

    public $footer = ['TOTAL:'];

    public function styles(Worksheet $sheet)
    {
        $sheet->getRowDimension('1')->setRowHeight(85, 'px');
        $sheet->getStyle('A1:H1')->applyFromArray([
            ...$this->font,
            ...$this->centerAlignment,
            ...$this->colorFiller,
            ...$this->borders,
        ]);

        $lastCell = $this->mergeCellsOfDepartmentsAndOutline($sheet);
        $sheet->getStyle('A1:A'.$lastCell)->applyFromArray([
            ...$this->colorFiller,
            ...$this->font,
            ...$this->centerAlignment,

        ]);
        $sheet->getStyle('G1:G'.$lastCell)->applyFromArray([
            ...$this->centerAlignment,

        ]);
        $sheet->getStyle('H1:H'.$lastCell)->applyFromArray([
            ...$this->centerAlignment,
        ]);
        $sheet->getStyle('A'.$lastCell + 1 .':H'.$lastCell + 1)->applyFromArray([
            ...$this->colorFiller,
            ...$this->font,
            ...$this->centerAlignment,
            ...$this->borders,
        ]);
        array_map(fn ($letter) => $sheet->getStyle($letter.$lastCell + 1)->getNumberFormat()->setFormatCode(
            '#,##;#,##;"-";'
        ), ['B', 'C', 'D', 'E']);
        $sheet->getStyle('F2:F'.$lastCell + 1)->getNumberFormat()->setFormatCode(
            '_("€"* #,##0_);_("€"* \(#,##0\);_("€"* "-"??_);_(@_)'
        );
        $sheet->getStyle('H2:H'.$lastCell + 1)->getNumberFormat()->setFormatCode(
            '_("€"* #,##0_);_("€"* \(#,##0\);_("€"* "-"??_);_(@_)'
        );
    }

    private function mergeCellsOfDepartmentsAndOutline($sheet, $startCell = 2): int
    {
        foreach ($this->deps as $dep) {
            $endCell = $startCell + count($dep) - 1;
            $sheet->mergeCells("A{$startCell}:A{$endCell}"); // department
            $sheet->mergeCells("G{$startCell}:G{$endCell}"); // wr, dept
            $sheet->mergeCells("H{$startCell}:H{$endCell}"); // sales, dept
            $sheet->getStyle("A{$startCell}:H{$endCell}")->applyFromArray([
                ...$this->borders,
            ]);
            $startCell = $endCell + 1;
        }

        return $endCell;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 15,
            'C' => 12,
            'D' => 12,
            'E' => 12,
            'F' => 15,
            'G' => 10,
            'H' => 15,
        ];
    }

    protected function countAmountOfCompaniesAssignedToUser($userId): int
    {
        return count(array_filter(
            cache('company_id_title_map'),
            fn ($company) => $company['ASSIGNED_BY_ID'] === $userId
        ));
    }

    protected function prepareDeps()
    {
        $preparedDeps = [];
        foreach ($this->deps as $depName => $userReqs) {
            foreach ($userReqs as $userId => $userData) {
                $user = $this->shortenUserName(
                    cache('realName_id')[$userId]['NAME']
                );
                $preparedDeps[] = [
                    strtoupper($depName),
                    $user,
                    $userData['amountOfClients'],
                    $userData['amountOfReqs'],
                    $userData['amountOfSales'],
                    $userData['sumOfSales'],
                    $this->countAmountForDep('amountOfReqs', $userReqs),
                    $this->countAmountForDep('sumOfSales', $userReqs),
                ];
            }
        }

        return $preparedDeps;
    }

    public function countAmountForDep($whatToCount, array $userReqs): int
    {
        return array_reduce($userReqs, fn ($cur, $v) => $cur + $v[$whatToCount], 0);
    }

    public function array(): array
    {
        return [
            $this->header,
            $this->prepareDeps(),
            $this->footer,
        ];
    }
}
