<?php

namespace App\Exports;

trait ExportStyles
{
    public $colorFiller = [
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => [
                'rgb' => 'dbdbdb',
            ],
        ],
    ];

    public $font = [
        'font' => [
            'bold' => true,
            'name' => 'Helvetica Neue',
            'size' => '14',
        ],
    ];

    public $centerAlignment = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrapText' => true,
        ],
    ];

    public $borders = [
        'borders' => [
            'outline' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
    ];
}
