<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class B24 extends Facade
{
    /**
     * B24 binding
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'B24';
    }
}
