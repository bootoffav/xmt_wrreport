<?php

namespace App\Providers;

use App\B24\B24;
use Illuminate\Support\ServiceProvider;

class B24ServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('B24', B24::class);
    }
}
