<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapFive();
        View::share('realName_id', cache('realName_id'));
        View::composer(
            '*', 'App\Http\ViewComposers\DetailComposer'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {}
}
