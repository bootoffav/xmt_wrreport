<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    protected function scheduleTimezone()
    {
        return 'Europe/Moscow';
    }

    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sendRequests')->weeklyOn(5, '23:59');
        $schedule->command('sendRequests --period=month')->lastDayOfMonth('23:59');
        $schedule->command('sendDepartmentReport')->lastDayOfMonth('23:59');

        // updates
        $schedule->command('cache UserNames')->dailyAt('02:00');
        $schedule->command('cache CompanyNames')->dailyAt('02:03');
        $schedule->command('updateDepartments')->dailyAt('02:10');
        $schedule->command('cache Requests')->dailyAt('02:13');
        $schedule->command('findRequestHandling')->dailyAt('03:10');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
