<?php

namespace App\Console\Commands;

use App\WebRequestHandlingFinder;
use Illuminate\Console\Command;

class FindRequestHandling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'findRequestHandling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finds when requests are handled by responsible person';

    /**
     * Execute the console command.
     */
    public function handle(WebRequestHandlingFinder $wrHandlingFinder)
    {
        $wrHandlingFinder->handle();
    }
}
