<?php

namespace App;

class Company
{
    public function __construct()
    {
        $this->company_id_title_map = cache('company_id_title_map');
        $requests = $this->getRequests();
        // filter case-insensitively by company name in ASC mode
        ksort($requests, SORT_FLAG_CASE | SORT_STRING | SORT_NATURAL);
        $this->requests = $requests;
    }

    protected function getRequests(): array
    {
        $requests = WebRequest::getRequests();
        $companies = [];

        foreach ($requests as $r) {
            if (is_array($r->UF_CRM_TASK)) {
                $compOfReq = array_filter($r->UF_CRM_TASK, function ($value) {
                    return strpos($value, 'CO_') === 0;
                });
                foreach ($compOfReq as $companyUFId) {
                    try {
                        $companies[
                            $this->company_id_title_map[substr($companyUFId, 3)]['TITLE']
                        ][] = $r;
                    } catch (\ErrorException $e) {
                        continue;
                    }
                }
            }
        }

        return $companies;
    }
}
