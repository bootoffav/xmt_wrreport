<?php

namespace App;

use App\Helpers\DateFilter;
use App\Helpers\Formatters as fmt;
use App\Helpers\SaleFilter;
use Carbon\Carbon;

class RequestPeriodComparison
{
    public $curSummary = [
        'wr' => [],
        'wff' => [],
        'email' => [],
        'p' => [],
        'np' => [],
        'withProducts' => [],
        'sales' => 0,
        'saleReqs' => [],
    ];

    public $prevSummary = [
        'wr' => [],
        'wff' => [],
        'email' => [],
        'p' => [],
        'np' => [],
        'withProducts' => [],
        'sales' => 0,
        'saleReqs' => [],
    ];

    public $fd;

    public $td;

    public $curPeriod;

    public $prevPeriod;

    public $requests;

    public $prevRequests;

    public $dynamics = [
        'wr' => [],
        'email' => [],
        'p' => [],
        'np' => [],
        'withProducts' => [],
        'sales' => [],
    ];

    public function __construct(
        array $requestsForCurSummary,
        ?array $requestsForPrevSummary = null,
        ?string $respId = null
    ) {
        $period = session('datefilter');
        [$fd, $td] = self::getFromAndTo($period);

        $this->fd = $fd->format('d M Y');
        $this->td = $td->format('d M Y');

        [$pf, $pt] = $this->getPrevPeriodDates($period, $fd, $td);
        [
            $this->curPeriod,
            $this->prevPeriod
        ] = $this->getCurPrevPeriodAsStrings($period, $fd, $td, $pf, $pt);
        $this->requests = $this->curSummary($requestsForCurSummary, $fd, $td);
        $this->prevRequests = $this->prevSummary($requestsForPrevSummary ?? cache('allWebRequests', []), $pf, $pt);
        $this->getSales(
            $fd,
            $td,
            $pf,
            $pt,
            $respId,
            $requestsForCurSummary,
            $requestsForPrevSummary
        );
        $this->formDynamics();
    }

    protected function getSales(
        Carbon $fd,
        Carbon $td,
        Carbon $pf,
        Carbon $pt,
        ?string $respId,
        ?array $requestsForCurSales,
        ?array $requestsForPrevSales
    ) {

        $reqs = (new WebRequest(false))->requests;
        if ($respId) {
            $reqs = array_filter(
                $reqs,
                fn ($r) => $r->RESPONSIBLE_ID === $respId
            );
        }
        $curSales = (new SaleFilter($requestsForCurSales ?: $reqs))->requests;
        $prevSales = (new SaleFilter($requestsForPrevSales ?: $reqs, [$pf, $pt]))->requests;

        foreach ($curSales as $r) {
            $this->addSale('c', $r, [$fd->year, $td->year]);
        }

        foreach ($prevSales as $r) {
            $this->addSale('p', $r, [$pf->year, $pt->year]);
        }
    }

    public function __get($prop)
    {
        switch ($prop) {
            case 'prevRequestsAmount':
                return count($this->prevSummary['wr']) + count($this->prevSummary['email']);
        }
    }

    protected function formDynamics()
    {
        $deviation = count($this->curSummary['wr']) - count($this->prevSummary['wr']);
        $percents = round($deviation / (count($this->prevSummary['wr']) ?: 1) * 100);
        $this->dynamics['wr']['deviation_int'] = $deviation;
        $this->dynamics['wr']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = count($this->curSummary['email']) - count($this->prevSummary['email']);
        $percents = round($deviation / (count($this->prevSummary['email']) ?: 1) * 100);
        $this->dynamics['email']['deviation_int'] = $deviation;
        $this->dynamics['email']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = count($this->requests) - $this->prevRequestsAmount;
        $percents = round($deviation / ($this->prevRequestsAmount ?: 1) * 100);
        $this->dynamics['all']['deviation_int'] = $deviation;
        $this->dynamics['all']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = count($this->curSummary['p']) - count($this->prevSummary['p']);
        $percents = round($deviation / (count($this->prevSummary['p']) ?: 1) * 100);
        $this->dynamics['p']['deviation_int'] = $deviation;
        $this->dynamics['p']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = count($this->curSummary['np']) - count($this->prevSummary['np']);
        $percents = round($deviation / (count($this->prevSummary['np']) ?: 1) * 100);
        $this->dynamics['np']['deviation_int'] = $deviation;
        $this->dynamics['np']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = count($this->curSummary['withProducts']) - count($this->prevSummary['withProducts']);
        $percents = round($deviation / (count($this->prevSummary['withProducts']) ?: 1) * 100);
        $this->dynamics['withProducts']['deviation_int'] = $deviation;
        $this->dynamics['withProducts']['deviation_formatted'] = sprintf('%+d', $deviation).' ('.sprintf('%+d', $percents).'%)';

        $deviation = $this->curSummary['sales'] - $this->prevSummary['sales'];
        $percents = round($deviation / ($this->prevSummary['sales'] ?: 1) * 100);
        $this->dynamics['sales']['deviation_int'] = $deviation;
        $this->dynamics['sales']['deviation_formatted'] =
            (new \NumberFormatter('en_US', \NumberFormatter::CURRENCY))->formatCurrency($deviation, 'USD')
            .' ('.sprintf('%+d', $percents).'%)';
    }

    public static function getRequestType($r)
    {
        if (substr($r->TITLE, 0, 15) === 'Web form filled') {
            return 'wff';
        }
        if (str_contains($r->DESCRIPTION, 'Name:')) {
            return 'wr';
        }

        return 'email';
    }

    protected function curSummary(
        array $reqs,
        Carbon $fd,
        Carbon $td
    ) {
        $curRequests = [];
        foreach ($reqs as $r) {
            $r->type = self::getRequestType($r);
            $this->curSummary[$r->type][] = $r;
            $this->curSummary[empty($r->companies) ? 'np' : 'p'][] = $r;
            $curRequests[] = $r;
        }
        $this->curSummary['withProducts'] = WebRequest::withProducts($reqs);

        return $curRequests;
    }

    /**
     * @param  $period  values: 'c', 'p'
     * @param  $validYears  represents which years allowed in sale to be add to whole sale
     */
    protected function addSale($period, $r, $validYears)
    {
        [$fromYear, $toYear] = $validYears;
        if (is_null($r->revenue)) {
            return;
        }
        foreach ($r->revenue as $revenue) {
            if ((bool) filter_var($revenue['year'], FILTER_VALIDATE_INT,
                ['min_range' => $toYear, 'max_range' => $fromYear]
            ) or $revenue['year'] === 'unknown') {
                switch ($period) {
                    case 'c':
                        $this->curSummary['sales'] += floatval($revenue['amount']);
                        $this->curSummary['saleReqs'][] = $r;
                        break;
                    case 'p':
                        $this->prevSummary['sales'] += floatval($revenue['amount']);
                        $this->prevSummary['saleReqs'][] = $r;
                        break;
                }
            }
        }
    }

    protected function getPrevPeriodDates(
        string $period,
        Carbon $fd,
        Carbon $td
    ): array {
        // when period is a month or when specified dates are start and end of same month
        if (
            $period === 'month'
            or
            ($fd->day === 1 && $td->isLastOfMonth() && $fd->isSameMonth($td))
        ) {
            $pf = $fd->copy()->subMonth()->firstOfMonth();
            $pt = $fd->copy()->subDay();
        } elseif (
            // when start date is start of month but end date is last day of not same month
            $fd->day === 1
            &&
            $td->isLastOfMonth()
        ) {
            $pf = $fd->copy()->subMonths($td->diffInMonths($fd) + 1);
            $pt = $fd->copy()->subDay();
        } else { // when arbitrary period specified
            $pf = $fd->copy()->subDays($td->diffInDays($fd) + 1);
            $pt = $fd->copy()->subDay();
        }

        return [
            $pf->setTime(0, 0, 0),
            $pt->setTime(23, 59, 59),
        ];
    }

    protected function prevSummary(
        array $reqs,
        Carbon $pf,
        Carbon $pt
    ) {
        $prevRequests = [];
        $reqs = DateFilter::apply($reqs, $pf, $pt);
        foreach ($reqs as $r) {
            $r->type = self::getRequestType($r);
            $this->prevSummary[$r->type][] = $r;
            $this->prevSummary[empty($r->companies) ? 'np' : 'p'][] = $r;
            $prevRequests[] = $r;
        }
        $this->prevSummary['withProducts'] = WebRequest::withProducts($reqs);

        return $prevRequests;
    }

    protected function getCurPrevPeriodAsStrings(
        string $period,
        Carbon $fd,
        Carbon $td,
        Carbon $pf,
        Carbon $pt
    ): array {
        if (
            $period === 'month'
            or
            ($fd->day === 1 && $td->isLastOfMonth() && $fd->isSameMonth($td))
        ) {
            $cur = "{$fd->englishMonth} {$fd->year}";
            $prev = "{$pf->englishMonth} {$pf->year}";

            return [$cur, $prev];
        }
        if (
            // when start date is start of month but end date is last day of not same month
            $fd->day === 1
            &&
            $td->isLastOfMonth()
        ) {
            $cur = fmt::formatDate($period);
            $prev = "{$pf->englishMonth} {$pf->year} - {$pt->englishMonth} {$pt->year}";

            return [$cur, $prev];
        }

        // when arbitrary period specified
        $cur = fmt::formatDate($period);
        $prev = "{$pf->format('d M Y')} - {$pt->format('d M Y')}";

        return [$cur, $prev];

    }

    /**
     * @return [Carbon, Carbon]
     */
    public static function getFromAndTo(string $period): array
    {
        if (str_contains($period, ':')) {
            [$fd, $td] = explode(':', $period);
            $fd = Carbon::createFromFormat('Y-m-d', $fd);
            $td = Carbon::createFromFormat('Y-m-d', $td);
        } else {
            $fd = (Carbon::now())->firstOfMonth();
            $td = Carbon::now();
        }

        return [
            $fd->setTime(0, 0, 0),
            $td->setTime(23, 59, 59),
        ];
    }
}
