<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class RequestsOfEntity extends Mailable
{
    use Queueable, SerializesModels;

    // RequestPeriodComparison;
    public $rpc;

    /**
     * Create a new message instance.
     */
    public function __construct(
        public $requests,
        protected $entityName,
        public $path,
        $rpc = null
    ) {
        $this->rpc = $rpc;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Requests Of '.$this->entityName,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.RequestsOfEntity',
            with: [
                'counter' => 1,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
