<?php

namespace App\Mail;

use App\Helpers\DateFilter;
use App\Helpers\Formatters;
use App\Http\Controllers\Traits\FilterRequestListTrait;
use App\Http\Controllers\Traits\HelperTrait;
use App\RequestPeriodComparison;
use App\WebRequest;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class RequestsForPeriod extends Mailable
{
    use DateFilter, FilterRequestListTrait, HelperTrait, Queueable, SerializesModels;

    public $rpc;

    public $subject;

    public $period;

    public $totalHandlingTime;

    public $avg;

    public $isAnnualReport;

    public function __construct(
        ?string $period,
        protected Request $r
    ) {
        $this->period = match (true) {
            str_contains($period, ':') => $period,
            $period === 'month' => Carbon::now()->startOfMonth()->toDateString().':'.Carbon::now()->endOfMonth()->toDateString(),
            default => Carbon::now()->subDays('6')->toDateString().':'.Carbon::now()->toDateString()
        };

        session(['datefilter' => $this->period]);

        $wr = new WebRequest;
        $this->rpc = new RequestPeriodComparison($wr->requests);
        $this->isAnnualReport = $this->isAnnualReport();
        [$this->totalHandlingTime, $this->avg] = $this->handlingTime($wr->requests);
    }

    public function envelope(): Envelope
    {
        $from = Formatters::formatDate($this->rpc->fd);
        $to = Formatters::formatDate($this->rpc->td);

        return new Envelope(
            subject: $this->getSubject()
        );
    }

    public function getSubject(): string
    {
        [$from, $to] = explode(':', $this->period);
        $from = Carbon::createFromFormat('Y-m-d', $from)->format('d M Y');
        $to = Carbon::createFromFormat('Y-m-d', $to)->format('d M Y');

        return "Web requests ($from - $to)";
    }

    public function getRequests()
    {
        return $this->isRequestsShouldBeFiltered() ? $this->getRequestList($this->rpc->requests) : $this->rpc->requests;
    }

    public function content(): Content
    {
        return new Content(
            view: 'emails.RequestsForPeriod',
            with: [
                'counter' => 1,
                'requests' => $this->getRequests(),
            ]
        );
    }

    public function getUniqueResponsibles()
    {
        return array_unique(array_map(fn ($r) => $r->RESPONSIBLE_ID, $this->getRequests()));
    }
}
