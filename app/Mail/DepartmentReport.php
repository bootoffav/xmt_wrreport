<?php

namespace App\Mail;

use App\Exports\DepartmentReportExport;
use App\WebRequest;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Attachment;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class DepartmentReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(string $period)
    {
        $this->period = Carbon::createFromFormat('Y-m-d', $period)->format('F Y');
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: "XM - {$this->period} Web-requests by Departments",
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.DepartmentReport',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {

        return [
            Attachment::fromData(
                fn () => Excel::raw(
                    new DepartmentReportExport(new WebRequest),
                    \Maatwebsite\Excel\Excel::XLSX
                ),
                "XM - {$this->period} Web-requests by Departments.xlsx"
            )->withMime('application/vnd.ms-excel'),
        ];
    }
}
