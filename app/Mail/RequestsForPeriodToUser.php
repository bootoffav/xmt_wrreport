<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use App\RequestPeriodComparison;
use App\Helpers\DateFilter;
use App\Http\Controllers\Traits\HelperTrait;
use Carbon\Carbon;

class RequestsForPeriodToUser extends Mailable
{
    use Queueable, SerializesModels, HelperTrait, DateFilter;

    public $rpc;

    public $isAnnualReport;
    
    public $avg;

    public $totalHandlingTime;

    /**
     * Create a new message instance.
     */
    public function __construct(
        public $subject,
        string $respId,
        public $requests,
        public $prevRequests,
        ?string $period
    ) {

        $this->period = match (true) {
            str_contains($period, ':') => $period,
            $period === 'month' => Carbon::now()->startOfMonth()->toDateString().':'.Carbon::now()->endOfMonth()->toDateString(),
            default => Carbon::now()->subDays('6')->toDateString().':'.Carbon::now()->toDateString()
        };

        session(['datefilter' => $this->period]);

        $this->rpc = new RequestPeriodComparison($requests, $prevRequests, $respId);
        $this->isAnnualReport = $this->isAnnualReport();
        [$this->totalHandlingTime, $this->avg] = $this->handlingTime($requests);
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: $this->subject,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.RequestsForPeriodToUser',
            with: [
                'counter' => 1,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
