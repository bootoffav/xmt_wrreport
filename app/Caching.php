<?php

namespace App;

use App\Helpers\SaleFilter;

class Caching
{
    public static $CACHE_TTL = 1296000; // 15 days

    public static function cacheRequests($limit = null)
    {
        $requests = \B24::getAllWebRequests($limit);
        \Log::emergency('base web-requests data received');
        foreach ($requests as &$r) {
            if (! isset($r->UF_CRM_TASK) || gettype($r->UF_CRM_TASK) !== 'array') {
                $r->UF_CRM_TASK = [];
            }
            usleep(700_000);
            $r->country = static::setCountry($r);
            $r->revenue = static::setRevenue($r);
            $r->brands = static::setBrand($r);
            $r->products = static::setProduct($r);
            $r->companies = static::setCompany($r);
        }

        //count the difference
        if (\Cache::has('allWebRequests')) {
            $newWebRequestsAmount = count($requests) - count(cache('allWebRequests'));
            cache(['newWebRequestsAmount' => $newWebRequestsAmount], static::$CACHE_TTL);
        }
        cache(['allWebRequests' => $requests], static::$CACHE_TTL);
    }

    public static function cacheCompanyNames()
    {
        \Cache::put('company_id_title_map', \B24::get_company_id_title_map(), static::$CACHE_TTL);
        \Log::emergency('company cached');
    }

    public static function cacheUserNames()
    {
        \Cache::put('realName_id', \B24::getUserRealName(), static::$CACHE_TTL);
        \Log::emergency('users cached');
    }

    protected static function setCountry($r)
    {
        if (strpos($r->TITLE, '(') === false) {
            return null;
        } // in case no country information at all
        if ((strlen($r->TITLE) - 1) === strpos($r->TITLE, ')')) {
            return null;
        } // in case those braces are for date at the end of string
        $start = strpos($r->TITLE, '(') + 1;
        $length = strpos($r->TITLE, ')') - $start;
        $countryCandidate = substr($r->TITLE, $start, $length);
        if (strpos($countryCandidate, '(') !== false) {
            $countryCandidate .= ')';
        } // in case there is abbreviation using opening paranthesise

        return $countryCandidate;
    }

    protected static function setRevenue($r)
    {
        if (SaleFilter::is_sale_request($r)) {
            $revenue = \B24::getRevenue($r->ID);
            if (is_array($revenue)) {
                return self::parseRevenue($revenue);
            }
        }

        return null;
    }

    private static function parseRevenue($revenue)
    {
        $parsedRevenue = [];
        foreach ($revenue as $revenueRawString) {
            if (strncmp($revenueRawString, '$', 1) === 0) {

                $revenueRawString = str_ireplace("'", '', $revenueRawString);
                preg_match('/[[:digit:]]+[.,]?[[:digit:]]+/', $revenueRawString, $amount);
                $amount = empty($amount) ? 0 : str_replace(',', '.', trim($amount[0]));

                preg_match('/ 20\d\d/', $revenueRawString, $year);
                $year = empty($year) ? 'unknown' : trim($year[0]);

                $parsedRevenue[] = [
                    'amount' => $amount,
                    'year' => $year,
                ];
            }
        }

        return $parsedRevenue;
    }

    protected static function setBrand($r)
    {
        if (is_array($r->UF_CRM_TASK)) {
            $brands = array_values(array_intersect($r->UF_CRM_TASK, config('app.b24_brand_filter')));
            if (empty($brands)) {
                return [];
            }
            foreach ($brands as $brand) {
                $brand_names[] = array_search($brand, config('app.b24_brand_filter'));
            }
        }

        return $brand_names ?? null;
    }

    protected static function setCompany($r): array
    {
        $company_id_title_map = cache('company_id_title_map');

        return (is_array($r->UF_CRM_TASK)) ?
            array_reduce(
                array_filter($r->UF_CRM_TASK, fn ($v) => strpos($v, 'CO_') === 0),
                function ($companies, $id) use ($company_id_title_map) {
                    $id = substr($id, 3);
                    try {
                        $companies[$id] = trim($company_id_title_map[$id]['TITLE']);
                    } catch (\ErrorException $e) {
                    }

                    return $companies;
                },
                []
            ) : [];
    }

    protected static function setProduct($r): ?array
    {
        if (is_array($r->UF_CRM_TASK)) {
            $products = array_values(array_intersect($r->UF_CRM_TASK, config('app.b24_product_filter')));
            foreach ($products as $product) {
                $product_names[] = array_search($product, config('app.b24_product_filter'));
            }
        }

        return $product_names ?? static::getProductFromDescription($r->DESCRIPTION);
    }

    public static function getProductFromDescription(string $desc): ?array
    {
        $DELIMETER = '****************************************************';
        $first_delimeter_pos = strpos($desc, $DELIMETER);
        $last_delimeter_pos = strrpos($desc, $DELIMETER);
        if ($first_delimeter_pos === false) {
            return null;
        }
        if ($first_delimeter_pos !== $last_delimeter_pos) {
            $s = trim(substr(
                $desc,
                $first_delimeter_pos + strlen($DELIMETER),
                $last_delimeter_pos - $first_delimeter_pos - strlen($DELIMETER)
            ));

            $res = preg_match('/^.*(?=[[:blank:]]+-[[:blank:]])/', $s, $match);
            if ($res === 0) {
                preg_match('/.*(?=\|)/', $s, $match);
            }
        }

        return isset($match[0]) ? [trim($match[0])] : [];
    }
}
