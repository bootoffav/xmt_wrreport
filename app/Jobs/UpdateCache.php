<?php

namespace App\Jobs;

use App\Caching;
use App\Mail\CacheUpdated;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class UpdateCache implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected string $email, protected $limit = null) {}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Caching::cacheUserNames();
        sleep(4);
        Caching::cacheCompanyNames();
        sleep(4);
        Caching::cacheRequests($this->limit);
        sleep(4);
        Artisan::call('findRequestHandling');
        Mail::to($this->email)->send(
            new CacheUpdated
        );
    }
}
