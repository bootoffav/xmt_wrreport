<?php

namespace App;

use App\Helpers\UFFilter;

class Product extends UFFilter
{
    public function __construct()
    {
        $requests = WebRequest::getRequests();
        $this->filter = config('app.b24_product_filter');
        foreach ($requests as $r) {
            if (is_null($r->products)) {
                continue;
            }
            foreach ($r->products as $p) {
                isset($this->filter[$p]) ?: $this->filter[$p] = null;
            }
        }
        parent::__construct($requests);
        ksort($this->requests, SORT_STRING);
        if (array_key_exists('', $this->requests)) {
            $this->requests['No product'] = $this->requests[''];
            unset($this->requests['']);
        }
    }
}
