#!/bin/bash
sudo -u www-data php artisan down --refresh=30

sudo chown bootoffav:bootoffav . -R
git pull

# frontend build
yarn
yarn build

#backend build
composer install
php artisan view:cache
php artisan optimize
composer dump-autoload


sudo chown www-data:www-data . -R
sudo -u www-data php artisan up